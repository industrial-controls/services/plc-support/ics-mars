package ch.cern.be.ics.plcinfo.userinfo.dto;

public class SSOBean {
    private final String userId;
    private final String userLogin;
    private final String userName;

    public SSOBean(String userId, String userLogin, String userName) {
        this.userId = userId;
        this.userLogin = userLogin;
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getUserName() {
        return userName;
    }
}
