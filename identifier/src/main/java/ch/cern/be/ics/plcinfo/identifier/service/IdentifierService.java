package ch.cern.be.ics.plcinfo.identifier.service;

import ch.cern.be.ics.plcinfo.identifier.dto.EquipmentIdentifier;
import ch.cern.be.ics.plcinfo.infoream.dto.general.InforEamInfo;
import ch.cern.be.ics.plcinfo.infoream.service.InforEamService;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.Optional;

@Named
public class IdentifierService {

    private final InforEamService inforEamService;
    private final LanDbJDBCService lanDbJDBCService;

    @Inject
    public IdentifierService(InforEamService inforEamService, LanDbJDBCService lanDbJDBCService) {
        this.inforEamService = inforEamService;
        this.lanDbJDBCService = lanDbJDBCService;
    }

    /**
     * Query InforEAM to get potential functional position name from the asset code.
     * Next query LanDB to obtain potential aliases for the device.
     *
     * @param code asset code recognizable by InforEAM
     * @return EquipmentIdentifier DTO with list of possible identifiers or empty optional if asset was not found in InforEAM.
     */
    public Optional<EquipmentIdentifier> getDeviceIdentifierFromAsset(String code) {
        return inforEamService.getDeviceInfoFromAssetCode(code.toUpperCase())
                .map(InforEamInfo::getName)
                .map(this::getDeviceIdentifiersFromFunctionalPosition)
                .orElse(null);
    }

    /**
     * Query LanDB to obtain potential aliases for the device.
     *
     * @param code functional position name recognizable by Lan DB
     * @return EquipmentIdentifier DTO with list of possible identifiers. If nothing was found we put the initial code as result.
     */
    public Optional<EquipmentIdentifier> getDeviceIdentifiersFromFunctionalPosition(String code) {
        code = code.toUpperCase();
        EquipmentIdentifier result = lanDbJDBCService.getPotentialNames(code)
                .map(EquipmentIdentifier::new)
                .orElse(new EquipmentIdentifier(Collections.singletonList(code)));
        return Optional.of(result);
    }
}
