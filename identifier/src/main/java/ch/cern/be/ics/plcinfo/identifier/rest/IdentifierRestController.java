package ch.cern.be.ics.plcinfo.identifier.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.identifier.dto.EquipmentIdentifier;
import ch.cern.be.ics.plcinfo.identifier.service.IdentifierService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/identifier")
public class IdentifierRestController {
    private final IdentifierService identifierService;

    @Inject
    public IdentifierRestController(IdentifierService identifierService) {
        this.identifierService = identifierService;
    }

    @CrossOrigin
    @GetMapping("/asset")
    public ResponseEntity<EquipmentIdentifier> getInforDataForAssetCode(@RequestParam String code) {
        return Utils.formResponse(identifierService.getDeviceIdentifierFromAsset(code));
    }

    @CrossOrigin
    @GetMapping("/position")
    public ResponseEntity<EquipmentIdentifier> getInforDataForFunctionalPosition(@RequestParam String code) {
        return Utils.formResponse(identifierService.getDeviceIdentifiersFromFunctionalPosition(code));
    }
}
