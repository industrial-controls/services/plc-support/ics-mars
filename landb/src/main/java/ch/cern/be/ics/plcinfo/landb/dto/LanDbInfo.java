package ch.cern.be.ics.plcinfo.landb.dto;

public class LanDbInfo {
    private final String maker;
    private final String hardwareType;
    private final String genericType;
    private final String responsiblePerson;
    private final String mainUser;
    private final String location;
    private final String group;
    private final String description;
    private final String outlet;
    private final String outletId;
    private final String hostName;

    public LanDbInfo() {
        this("N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A");
    }

    public LanDbInfo(String maker, String hardwareType, String genericType, String responsiblePerson, String mainUser, String location, String group, String description, String outlet, String outletId, String hostName) {
        this.maker = maker;
        this.hardwareType = hardwareType;
        this.genericType = genericType;
        this.responsiblePerson = responsiblePerson;
        this.mainUser = mainUser;
        this.location = location.replaceFirst(" ", "/");
        this.group = group;
        this.description = description;
        this.outlet = outlet;
        this.outletId = outletId;
        this.hostName = hostName;
    }

    public String getMaker() {
        return maker;
    }

    public String getHardwareType() {
        return hardwareType;
    }

    public String getGenericType() {
        return genericType;
    }

    public String getResponsiblePerson() {
        return responsiblePerson;
    }

    public String getMainUser() {
        return mainUser;
    }

    public String getLocation() {
        return location;
    }

    public String getGroup() {
        return group;
    }

    public String getDescription() {
        return description;
    }

    public String getOutlet() {
        return outlet;
    }

    public String getOutletId() {
        return outletId;
    }

    public String getHostName() { return hostName;    }

}
