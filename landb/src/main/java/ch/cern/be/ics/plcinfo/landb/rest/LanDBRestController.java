package ch.cern.be.ics.plcinfo.landb.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/landb")
public class LanDBRestController {

    private final LanDbJDBCService lanDbJDBCService;

    @Inject
    public LanDBRestController(LanDbJDBCService lanDbJDBCService) {
        this.lanDbJDBCService = lanDbJDBCService;
    }

    @CrossOrigin
    @GetMapping("/general")
    public ResponseEntity<LanDbInfo> getDeviceInfo(@RequestParam String[] code) {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(lanDbJDBCService::getDeviceInfo, Arrays.stream(code)));
    }
}
