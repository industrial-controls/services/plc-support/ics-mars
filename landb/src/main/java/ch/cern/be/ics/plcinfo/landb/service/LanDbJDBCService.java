package ch.cern.be.ics.plcinfo.landb.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Named
public class LanDbJDBCService extends BaseJdbcService {

    @Inject
    LanDbJDBCService(PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.CERN_DB1, logger, proxy);
    }

    @Override
    protected String getQuery() {
        return "select nd.MAKER, " +
                "nd.HARDWARE_TYPE, " +
                "nd.GENERIC_TYPE, " +
                "pi.FIRST_NAME || ' ' || pi.SURNAME  || ' (' || pi.DIVISION || '-' || pi.CERN_GROUP || ') - Phone: ' || pi.PHONE_NUM1  as Responsible_Person,  " +
                "pip.FIRST_NAME || ' ' || pip.SURNAME || ' (' || pip.DIVISION || '-' || pip.CERN_GROUP || ') - Phone: ' || pip.PHONE_NUM1  as Main_User, " +
                "nd.BUILDING || ' ' ||  nd.FLOOR || '-' || nd.ROOM as location, " +
                "nd.GROUP_NAME, " +
                "nd.DESCRIPTION, " +
                "do.outlet_unique_ca as Outlet_Label,  " +
                "do.outlet_id as Outlet_Id,  " +
                "landb.nd.usual_name " +
                "from landb.network_device nd, landb.device_outlet do, landb.PERSONAL_INFORMATION_PUBLIC pi, landb.PERSONAL_INFORMATION_PUBLIC pip, LANDB.TCPIP_ADDRESS ta " +
                "where ta.id = landb.do.address_id  " +
                "and landb.nd.id = landb.ta.device_id " +
                "and landb.pi.LANDB_PERS_ID = landb.nd.CONTACT_LANDB_PERS_ID " +
                "and landb.pip.LANDB_PERS_ID = landb.nd.USER_LANDB_PERS_ID " +
                "and landb.nd.usual_name = ?";
    }

    public Optional<LanDbInfo> getDeviceInfo(String name) {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return Optional.of(new LanDbInfo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
            }
        } catch (SQLException e) {
            logger.warning("Error when collecting data from LanDB, " + e.getMessage());
            logger.warning(e);
            restartConnection();
        }
        return Optional.empty();
    }

    public Optional<List<String>> getPotentialNames(String name) {
        String query = "select name, alias from landb.tcpip_alias where name= ? or alias = ?";
        try {
            PreparedStatement preparedStatement = getPreparedQuery(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, name);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return Optional.of(Arrays.asList(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            logger.warning("Error when collecting data from LanDB, " + e.getMessage());
            logger.warning(e);
            restartConnection();
        }
        return Optional.empty();
    }
}
