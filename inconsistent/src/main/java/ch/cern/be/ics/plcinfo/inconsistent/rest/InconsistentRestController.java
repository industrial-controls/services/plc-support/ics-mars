package ch.cern.be.ics.plcinfo.inconsistent.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.inconsistent.dto.InconsistentData;
import ch.cern.be.ics.plcinfo.inconsistent.service.InconsistentJDBCService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

/**
 * REST API for inconsistency checks between PLC names in different data sources. This is not really needed or used by MARS, but had to be somewhere.
 * Eventually this might get removed and placed somewhere in ICEDM interface.
 */
@RestController
@RequestMapping("/api/inconsistent")
public class InconsistentRestController {

    private final InconsistentJDBCService inconsistentJdbcService;

    @Inject
    public InconsistentRestController(InconsistentJDBCService inconsistentJdbcService) {
        this.inconsistentJdbcService = inconsistentJdbcService;
    }

    @CrossOrigin
    @GetMapping("/data")
    public ResponseEntity<InconsistentData> getInconsistentData() {
        return Utils.formResponse(inconsistentJdbcService.getData());
    }
}
