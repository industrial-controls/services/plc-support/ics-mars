package ch.cern.be.ics.plcinfo.inconsistent.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.inconsistent.dto.InconsistentData;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Named
public class InconsistentJDBCService extends BaseJdbcService {

    @Inject
    protected InconsistentJDBCService(PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.ICESAS, logger, proxy);
    }

    @Override
    protected String getQuery() {
        return "select to_char(UPDATED_DATE,'YYYY-MM-DD HH24:MI:SS') LAST_UPDATE, DC_ID, RN, PROBLEM_DESC, DEVICE_NAME, DEVICE_TYPE, RESPONSIBLE, TXT01, TXT02, TXT03, TXT04, TXT05, TXT06, TXT07, TXT08, TXT09, TXT10 from ICEDM_MV_DATA_CONSISTENCY order by DC_ID,RN";
    }

    public Optional<InconsistentData> getData() {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            ResultSet results = preparedStatement.executeQuery();
            return Optional.of(parseResults(results)
                    .orElse(new InconsistentData(Collections.emptyList(), 0)));
        } catch (SQLException e) {
            logger.severe(e);
            return Optional.empty();
        }
    }

    private Optional<InconsistentData> parseResults(ResultSet results) {
        int resultSize = 18;
        try {
            List<List<String>> queryData = new ArrayList<>();
            while (results.next()) {
                List<String> entryData = new ArrayList<>();
                for (int i = 1; i < resultSize; i++) {
                    entryData.add(results.getString(i));
                }
                queryData.add(entryData);
            }
            return Optional.of(new InconsistentData(queryData, 1));
        } catch (SQLException e) {
            logger.severe(e);
        }
        return Optional.empty();
    }
}
