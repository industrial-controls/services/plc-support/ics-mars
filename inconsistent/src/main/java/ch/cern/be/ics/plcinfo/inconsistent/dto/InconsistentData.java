package ch.cern.be.ics.plcinfo.inconsistent.dto;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InconsistentData {
    private final Map<String, InconsistencyCheck> incosistencyChecks;

    public InconsistentData(List<List<String>> queryData, int checkTypeColumn) {
        incosistencyChecks = queryData
                .stream()
                .collect(Collectors.groupingBy(
                        singleCheckData -> singleCheckData.get(checkTypeColumn)
                ))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> new InconsistencyCheck(entry.getValue())
                ));
    }

    public Map<String, InconsistencyCheck> getIncosistencyChecks() {
        return incosistencyChecks;
    }
}
