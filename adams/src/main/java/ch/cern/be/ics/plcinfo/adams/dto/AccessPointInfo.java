package ch.cern.be.ics.plcinfo.adams.dto;

public class AccessPointInfo {
    private final AccessPoint accessPoint;
    private final AccessSituation accessSituation;

    public AccessPointInfo(AccessPoint accessPoint, AccessSituation accessSituation) {
        this.accessPoint = accessPoint;
        this.accessSituation = accessSituation;
    }

    public AccessPoint getAccessPoint() {
        return accessPoint;
    }

    public AccessSituation getAccessSituation() {
        return accessSituation;
    }

    public boolean isGranted() {
        return accessSituation.getAccesssituation().equals("Y");
    }
}
