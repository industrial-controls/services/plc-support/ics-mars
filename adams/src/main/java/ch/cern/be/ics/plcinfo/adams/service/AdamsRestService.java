package ch.cern.be.ics.plcinfo.adams.service;

import ch.cern.be.ics.plcinfo.adams.dto.AccessPoint;
import ch.cern.be.ics.plcinfo.adams.dto.AccessPointInfo;
import ch.cern.be.ics.plcinfo.adams.dto.AccessSituation;
import ch.cern.be.ics.plcinfo.adams.dto.AdamsInfo;
import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.service.RestQueryProxy;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;
import org.springframework.core.ParameterizedTypeReference;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Named
public class AdamsRestService {
    private final RestQueryProxy restQueryProxy = new RestQueryProxy("https://oraweb.cern.ch/ords/devdb11/adams3/api/");
    private final LanDbJDBCService lanDbJDBCService;
    private final PLCInfoLogger logger;

    @Inject
    public AdamsRestService(LanDbJDBCService lanDbJDBCService, PLCInfoLogger logger) {
        this.lanDbJDBCService = lanDbJDBCService;
        this.logger = logger;
    }

    public Optional<AdamsInfo> getAccessPointInfo(String personId, String functionalPosition) {
        List<AccessPoint> accessPointCodesForLocation = getLocation(functionalPosition)
                .map(this::getListOfAccessPoints)
                .orElse(Collections.emptyList());
        return Utils.collectAsynchronousData(() -> getAccessPointInfoForPoints(personId, accessPointCodesForLocation), "adams", logger);
    }

    private Optional<String> getLocation(String functionalPosition) {
        return Utils.collectAsynchronousData(() -> lanDbJDBCService.getDeviceInfo(functionalPosition), "adams", logger)
                .map(LanDbInfo::getLocation);
    }

    private List<AccessPoint> getListOfAccessPoints(String location) {
        return Utils.collectAsynchronousData(() -> getListOfAccessPointsForLocation(location), "adams", logger)
                .orElse(Collections.emptyList());
    }

    private Optional<AdamsInfo> getAccessPointInfoForPoints(String personId, List<AccessPoint> accessPointList) {
        List<AccessPointInfo> accessPoints = accessPointList
                .stream()
                .map(accessPointCode -> getLocationAccessForUser(accessPointCode, personId))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        if (accessPoints.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new AdamsInfo(accessPoints));
    }

    private Optional<List<AccessPoint>> getListOfAccessPointsForLocation(String location) {
        try {
            List<AccessPoint> accessPoints = restQueryProxy.queryParametrizedResult("/acp4loc/" + location, new ParameterizedTypeReference<Map<String, List<AccessPoint>>>() {
            }, map -> map.get("items"));
            if (accessPoints != null && !accessPoints.isEmpty()) {
                return Optional.of(accessPoints);
            }
        } catch (Exception ex) {
            logger.warning("Error when collecting data from adams. " + ex.toString());
        }
        return Optional.empty();
    }

    private Optional<AccessPointInfo> getLocationAccessForUser(AccessPoint accessPoint, String userId) {
        try {
            AccessSituation situation = restQueryProxy.query("/prs_at_acp/" + accessPoint.getAccesspointcode() + "/" + userId, AccessSituation.class, Function.identity());
            return Optional.of(new AccessPointInfo(accessPoint, situation));
        } catch (Exception e) {
            logger.warning("Error when collecting data from adams. " + e.toString());
        }
        return Optional.empty();
    }
}
