package ch.cern.be.ics.plcinfo.adams.rest;

import ch.cern.be.ics.plcinfo.adams.dto.AdamsInfo;
import ch.cern.be.ics.plcinfo.adams.service.AdamsRestService;
import ch.cern.be.ics.plcinfo.commons.Utils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/adams")
public class AdamsRestController {

    private final AdamsRestService adamsRestService;

    @Inject
    public AdamsRestController(AdamsRestService adamsRestService) {
        this.adamsRestService = adamsRestService;
    }

    @CrossOrigin
    @GetMapping("/access")
    public ResponseEntity<AdamsInfo> getAccessPointsInfo(@RequestParam String[] code, @RequestParam String userId) {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(c -> adamsRestService.getAccessPointInfo(userId, c), Arrays.stream(code)));
    }
}
