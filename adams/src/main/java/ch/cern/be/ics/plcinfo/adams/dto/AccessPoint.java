package ch.cern.be.ics.plcinfo.adams.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessPoint {
    private String accesspointcode;
    private String accesspointdescription;

    public String getAccesspointcode() {
        return accesspointcode;
    }

    public void setAccesspointcode(String accesspointcode) {
        this.accesspointcode = accesspointcode;
    }

    public String getAccesspointdescription() {
        return accesspointdescription;
    }

    public void setAccesspointdescription(String accesspointdescription) {
        this.accesspointdescription = accesspointdescription;
    }
}
