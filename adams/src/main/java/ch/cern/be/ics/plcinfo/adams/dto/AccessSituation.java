package ch.cern.be.ics.plcinfo.adams.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessSituation {
    private String accesssituation;
    private List<AccessPermissions> permissions;

    public AccessSituation() {
    }

    public AccessSituation(String accesssituation) {
        this.accesssituation = accesssituation;
    }

    public String getAccesssituation() {
        return accesssituation;
    }

    public void setAccesssituation(String accesssituation) {
        this.accesssituation = accesssituation;
    }

    public List<AccessPermissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<AccessPermissions> permissions) {
        this.permissions = permissions;
    }
}
