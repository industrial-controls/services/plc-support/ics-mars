package ch.cern.be.ics.plcinfo.commons;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.easymock.EasyMock.createNiceMock;
import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void tryDataUntilNonEmptyResult() {
        Function<Integer, Optional<Integer>> function = Optional::ofNullable;
        Stream<Integer> dataStream = Stream.of(null, 1, 2, 3);
        Optional<Integer> result = Utils.tryDataUntilNonEmptyResult(function, dataStream);
        assertTrue("We expect to find the first non empty value in the list", result.isPresent());
        assertEquals(1, result.get().intValue());
    }

    @Test
    public void tryDataUntilNonEmptyResultEmpty() {
        Function<Integer, Optional<Integer>> function = Optional::ofNullable;
        Stream<Integer> dataStream = Stream.of(null, null, null);
        Optional<Integer> result = Utils.tryDataUntilNonEmptyResult(function, dataStream);
        assertFalse("We expect to get no result since there will be no non-empty value", result.isPresent());
    }

    @Test
    public void formResponseCorrectData() {
        ResponseEntity<Integer> result = Utils.formResponse(Optional.of(1));
        assertEquals("We expect status 200 for non empty data", HttpStatus.OK, result.getStatusCode());
    }


    @Test
    public void formResponseMissingData() {
        ResponseEntity<Integer> result = Utils.formResponse(Optional.empty());
        assertEquals("We expect status 500 for empty data", HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
    }


    @Test
    public void collectAsynchronousData() {
        int value = 1;
        Supplier<Optional<Integer>> supplier = () -> Optional.of(value);
        Optional<Integer> result = Utils.collectAsynchronousData(supplier, "errorLabel", createNiceMock(PLCInfoLogger.class));
        assertTrue("We expect the data to be received", result.isPresent());
        assertEquals("The value should match the data from supplier", value, result.get().intValue());
    }


    @Test
    public void collectAsynchronousDataDefaultTimeout() {
        int value = 1;
        Supplier<Optional<Integer>> supplier = getSimpleSupplierWithTimeout(value, 150000);
        Optional<Integer> result = Utils.collectAsynchronousData(supplier, "errorLabel", createNiceMock(PLCInfoLogger.class));
        assertFalse("We expect the data to be missing", result.isPresent());
    }


    @Test
    public void collectAsynchronousDataCustomCallback() {
        int value = 1;
        Supplier<Optional<Integer>> supplier = () -> Optional.of(value);
        AtomicInteger errorCounter = new AtomicInteger(0);
        BiConsumer<Exception, String> errorCallback = (e, s) -> errorCounter.incrementAndGet();
        Optional<Integer> result = Utils.collectAsynchronousData(supplier, "errorLabel", errorCallback, 1);
        assertTrue("We expect the data to be received", result.isPresent());
        assertEquals("The value should match the data from supplier", value, result.get().intValue());
        assertEquals("Handler should have received no errors", 0, errorCounter.get());
    }

    @Test
    public void collectAsynchronousDataCustomCallbackTimeout() {
        int value = 1;
        Supplier<Optional<Integer>> supplier = getSimpleSupplierWithTimeout(value, 5000);
        AtomicInteger errorCounter = new AtomicInteger(0);
        BiConsumer<Exception, String> errorCallback = (e, s) -> errorCounter.incrementAndGet();
        Optional<Integer> result = Utils.collectAsynchronousData(supplier, "errorLabel", errorCallback, 1);
        assertFalse("We expect the data to be missing", result.isPresent());
        assertEquals("Handler should have received timeout error", 1, errorCounter.get());
    }

    @Test
    public void getFromFutureOrDefault() {
        int value = 1;
        int defaultValue = 2;
        Future<Optional<Integer>> future = getSimpleTimeoutingFuture(value, 10000);
        AtomicInteger errorCounter = new AtomicInteger(0);
        BiConsumer<Exception, String> errorCallback = (e, s) -> errorCounter.incrementAndGet();

        int result = Utils.getFromFutureOrDefault(future, defaultValue, "errorLabel", errorCallback, 1);

        assertEquals("We expect the call won't timeout and we'll receive the proper value", value, result);
        assertEquals("There should be no errors logged", 0, errorCounter.get());
    }

    @Test
    public void getFromFutureOrDefaultDefault() {
        int value = 1;
        int defaultValue = 2;
        Future<Optional<Integer>> future = getSimpleTimeoutingFuture(value, 1);
        AtomicInteger errorCounter = new AtomicInteger(0);
        BiConsumer<Exception, String> errorCallback = (e, s) -> errorCounter.incrementAndGet();

        int result = Utils.getFromFutureOrDefault(future, defaultValue, "errorLabel", errorCallback, 10);

        assertEquals("We expect the call to timeout and we'll receive the default value", defaultValue, result);
        assertEquals("There should be timeout error logged", 1, errorCounter.get());
    }

    private Supplier<Optional<Integer>> getSimpleSupplierWithTimeout(int value, int timeout) {
        return () -> {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Optional.of(value);
        };
    }

    private Future<Optional<Integer>> getSimpleTimeoutingFuture(int value, int maxTimeout) {
        return new Future<Optional<Integer>>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public Optional<Integer> get() {
                return Optional.of(value);
            }

            @Override
            public Optional<Integer> get(long timeout, TimeUnit unit) throws TimeoutException {
                if (timeout > maxTimeout) {
                    throw new TimeoutException("");
                } else {
                    return Optional.of(value);
                }
            }
        };
    }
}