package ch.cern.be.ics.plcinfo.commons;

public class Credentials {
    public static final String login = System.getenv("login");
    public static final String pass = System.getenv("pass");
    public static final String pass2 = System.getenv("pass2");
    public static final String icesasLogin = System.getenv("icesasLogin");
    public static final String icesasPassword = System.getenv("icesasPassword");
}
