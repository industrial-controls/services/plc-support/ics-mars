package ch.cern.be.ics.plcinfo.commons.service;

import ch.cern.be.ics.plcinfo.commons.Credentials;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class RestQueryProxy {
    private final String credentials = "Basic " + new String(Base64.getEncoder().encode((Credentials.login + ":" + Credentials.pass2).getBytes()));
    private final RestTemplate restTemplate = new RestTemplate();
    private final String basePath;

    public RestQueryProxy(String basePath) {
        this.basePath = basePath;
    }

    public <T, S> T query(String path, Class<S> returnType, Function<S, T> extractor, Supplier<Map<String, String>> headersSupplier) {
        ResponseEntity<S> response = restTemplate.exchange(basePath + path, HttpMethod.GET, prepareHeaders(headersSupplier), returnType);
        return extractor.apply(response.getBody());
    }

    public <T, S> T query(String path, Class<S> returnType, Function<S, T> extractor) {
        return query(path, returnType, extractor, getBasicAuthHeadersSupplier());
    }

    public <T, S> T queryParametrizedResult(String path, ParameterizedTypeReference<S> parameterType, Function<S, T> extractor, Supplier<Map<String, String>> headersSupplier) {
        ResponseEntity<S> response = restTemplate.exchange(basePath + path, HttpMethod.GET, prepareHeaders(headersSupplier), parameterType);
        return extractor.apply(response.getBody());
    }

    public <T, S> T queryParametrizedResult(String path, ParameterizedTypeReference<S> parameterType, Function<S, T> extractor) {
        return queryParametrizedResult(path, parameterType, extractor, Collections::emptyMap);
    }

    private HttpEntity<?> prepareHeaders(Supplier<Map<String, String>> headersSupplier) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headersSupplier.get()
                .forEach(headers::add);
        return new HttpEntity<>(headers);
    }

    public Supplier<Map<String, String>> getBasicAuthHeadersSupplier() {
        return () -> Collections.singletonMap("Authorization", credentials);
    }
}
