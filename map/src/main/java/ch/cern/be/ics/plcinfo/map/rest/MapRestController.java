package ch.cern.be.ics.plcinfo.map.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.map.dto.MapBean;
import ch.cern.be.ics.plcinfo.map.service.MapService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/map")
public class MapRestController {

    private final MapService mapService;

    @Inject
    public MapRestController(MapService mapService) {
        this.mapService = mapService;
    }

    @CrossOrigin
    @GetMapping("/location")
    public ResponseEntity<MapBean> getMapLink(@RequestParam String[] code) {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(mapService::getMapLink, Arrays.stream(code)));
    }
}
