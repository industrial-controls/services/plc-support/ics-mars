package ch.cern.be.ics.plcinfo.map.dto;

/**
 * Created by elmandil on 7/17/2017.
 */
public class MapBean {
    private final String mapLocation;

    public MapBean(String mapLocation) {
        this.mapLocation = mapLocation;
    }

    public String getMapLocation() {
        return mapLocation;
    }

}