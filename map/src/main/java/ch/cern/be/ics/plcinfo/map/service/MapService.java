package ch.cern.be.ics.plcinfo.map.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;
import ch.cern.be.ics.plcinfo.map.dto.MapBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Named
public class MapService extends BaseJdbcService {

    @Inject
    MapService(LanDbJDBCService lanDbJDBCService, PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.ICESAS, logger, proxy);
        this.lanDbJDBCService = lanDbJDBCService;
    }

    private final LanDbJDBCService lanDbJDBCService;

    public Optional<MapBean> getMapLink(String code) {
        String outletId = Utils.collectAsynchronousData(() -> lanDbJDBCService.getDeviceInfo(code), "landb", logger)
                .map(LanDbInfo::getOutletId)
                .orElse("");
        return getMapLink(code, outletId);
    }

    public Optional<MapBean> getMapLink(String code, String outletId) {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            preparedStatement.setString(1, code);
            ResultSet results = preparedStatement.executeQuery();
            if (results.next()) {
                return Optional.of(new MapBean(results.getString(1)));
            }
        } catch (SQLException ex) {
            logger.warning("Error when querying maps, " + ex.getMessage());
            logger.warning(ex);
            restartConnection();
        }
        return Optional.of(new MapBean(getOutletMapLink(outletId)));
    }

    private String getOutletMapLink(String outletId) {
        return "https://gis.cern.ch/gisportal/general.htm?it=['" + outletId + "']";
    }

    @Override
    protected String getQuery() {
        return "SELECT GIS_LINK FROM ICEDM_S_GEOSIP_EAM_EQUIPMENT WHERE CODE in (select OBJ_CODE from ICEDM_V_OBJECT_DEPEND where C1_CHILD = ?)";
    }
}
