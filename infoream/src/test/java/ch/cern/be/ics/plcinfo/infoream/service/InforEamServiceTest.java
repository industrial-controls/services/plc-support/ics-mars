package ch.cern.be.ics.plcinfo.infoream.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.infoream.dto.general.InforEamInfo;
import ch.cern.cmms.infor.wshub.Credentials;
import ch.cern.cmms.infor.wshub.Equipment;
import ch.cern.cmms.infor.wshub.Graph;
import ch.cern.cmms.infor.wshub.SOAPException;
import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

public class InforEamServiceTest {

    private InforEamService inforEamService;
    private PLCInfoLogger logger;
    private InforWebserviceProxy inforWebserviceProxy;

    @Before
    public void setUp() {
        logger = createNiceMock(PLCInfoLogger.class);
        inforWebserviceProxy = createNiceMock(InforWebserviceProxy.class);
        inforEamService = new InforEamService(logger, inforWebserviceProxy);
    }


    @Test
    public void getDeviceInfoFromCode() throws Exception {
        String equipmentCode = "equipmentCode";
        String name = "name";
        String department = "department";
        Capture<Credentials> credentialsCapture = Capture.newInstance();
        Equipment equipment = new Equipment();
        equipment.setHierarchyPositionCode(name);
        equipment.setCode(name);
        equipment.setDepartmentDesc(department);
        expect(inforWebserviceProxy.readEquipment(eq(equipmentCode), capture(credentialsCapture))).andReturn(equipment).atLeastOnce();
        expect(inforWebserviceProxy.readEquipment(eq(name), capture(credentialsCapture))).andReturn(equipment).atLeastOnce();
        expect(inforWebserviceProxy.readGraph(eq(name), anyObject())).andReturn(Optional.of(new Graph())).atLeastOnce();
        replay(logger, inforWebserviceProxy);

        Optional<InforEamInfo> result = inforEamService.getDeviceInfoFromAssetCode(equipmentCode);

        verify(logger, inforWebserviceProxy);
        assertTrue(result.isPresent());
        InforEamInfo data = result.get();
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.login, credentialsCapture.getValue().getUsername());
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.pass2, credentialsCapture.getValue().getPassword());
        assertEquals(name, data.getName());
        assertEquals(name, data.getCode());
        assertEquals(department, data.getDepartmentDesc());
    }

    @Test
    public void getDeviceInfoFromName() throws Exception {
        String name = "name";
        String department = "department";
        Capture<Credentials> credentialsCapture = Capture.newInstance();
        Equipment equipment = new Equipment();
        equipment.setHierarchyPositionCode(name);
        equipment.setDepartmentDesc(department);
        expect(inforWebserviceProxy.readEquipment(eq(name), capture(credentialsCapture))).andReturn(equipment).atLeastOnce();
        expect(inforWebserviceProxy.readGraph(anyString(), anyObject())).andReturn(Optional.of(new Graph())).atLeastOnce();
        replay(logger, inforWebserviceProxy);

        Optional<InforEamInfo> result = inforEamService.getDeviceInfoFromFunctionalPosition(name);

        verify(logger, inforWebserviceProxy);
        assertTrue(result.isPresent());
        InforEamInfo data = result.get();
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.login, credentialsCapture.getValue().getUsername());
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.pass2, credentialsCapture.getValue().getPassword());
        assertEquals(name, data.getName());
        assertEquals(department, data.getDepartmentDesc());
    }

    @Test
    public void getDeviceInfoFromNameFailed() throws Exception {
        String name = "name";
        Capture<Credentials> credentialsCapture = Capture.newInstance();
        expect(inforWebserviceProxy.readEquipment(eq(name), capture(credentialsCapture))).andThrow(new SOAPException()).anyTimes();
        replay(logger, inforWebserviceProxy);

        Optional<InforEamInfo> result = inforEamService.getDeviceInfoFromFunctionalPosition(name);

        verify(logger, inforWebserviceProxy);
        assertFalse(result.isPresent());
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.login, credentialsCapture.getValue().getUsername());
        assertEquals(ch.cern.be.ics.plcinfo.commons.Credentials.pass2, credentialsCapture.getValue().getPassword());
    }

}