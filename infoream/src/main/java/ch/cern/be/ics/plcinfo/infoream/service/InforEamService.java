package ch.cern.be.ics.plcinfo.infoream.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.infoream.dto.general.EquipmentComponents;
import ch.cern.be.ics.plcinfo.infoream.dto.general.InforEamInfo;
import ch.cern.be.ics.plcinfo.infoream.dto.workorder.WorkOrdersList;
import ch.cern.cmms.infor.wshub.Credentials;
import ch.cern.cmms.infor.wshub.Equipment;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;
import java.util.function.Function;

@Named
public class InforEamService {

    private final PLCInfoLogger logger;
    private final InforWebserviceProxy inforWebserviceProxy;
    private final Credentials credentials = new Credentials();
    private final InforEamGraphExtractor inforEamGraphExtractor;
    private final InforEamWorkOrdersExtractor inforEamWorkOrdersExtractor;

    @Inject
    InforEamService(PLCInfoLogger logger, InforWebserviceProxy inforWebserviceProxy) {
        this.logger = logger;
        this.inforWebserviceProxy = inforWebserviceProxy;
        System.setProperty("https.protocols", "TLSv1,SSLv3,SSLv2Hello");
        credentials.setUsername(ch.cern.be.ics.plcinfo.commons.Credentials.login);
        credentials.setPassword(ch.cern.be.ics.plcinfo.commons.Credentials.pass2);
        inforEamGraphExtractor = new InforEamGraphExtractor(inforWebserviceProxy::readGraph, credentials);
        inforEamWorkOrdersExtractor = new InforEamWorkOrdersExtractor(inforWebserviceProxy::readWorkOrdersLike, inforEamGraphExtractor::readGraphData, credentials);
    }

    public Optional<InforEamInfo> getDeviceInfoFromAssetCode(String equipmentCode) {
        return getDeviceInfoFromAssetCode(equipmentCode, this::readEquipmentData, this::getDeviceInfoFromFunctionalPosition);
    }

    Optional<InforEamInfo> getDeviceInfoFromAssetCode(String equipmentCode, Function<String, Optional<Equipment>> equipmentDataProvider,
                                                      Function<String, Optional<InforEamInfo>> deviceInfoFromFunctionPositionProvider) {
        return equipmentDataProvider.apply(equipmentCode)
                .map(Equipment::getHierarchyPositionCode)
                .map(deviceInfoFromFunctionPositionProvider)
                .orElse(Optional.empty());
    }

    public Optional<InforEamInfo> getDeviceInfoFromFunctionalPosition(String functionalPosition) {
        return getDeviceInfoFromFunctionalPosition(functionalPosition, this::readEquipmentData, inforEamGraphExtractor::readGraphData);
    }

    Optional<InforEamInfo> getDeviceInfoFromFunctionalPosition(String functionalPosition, Function<String, Optional<Equipment>> equipmentDataProvider, Function<String, EquipmentComponents> graphDataProvider) {
        return equipmentDataProvider.apply(functionalPosition)
                .map(eq -> new InforEamInfo(eq.getCode(), functionalPosition, eq.getDescription(), eq.getDepartmentDesc(), eq.getDepartmentCode(), graphDataProvider.apply(eq.getCode()), eq.getHierarchyPositionCode()));
    }

    private Optional<Equipment> readEquipmentData(String equipmentCode) {
        try {
            return Optional.of(inforWebserviceProxy.readEquipment(equipmentCode, credentials));
        } catch (Exception e) {
            logger.warning("Failed to access InforEAM. " + e.getMessage());
            inforWebserviceProxy.initializeConnection();
            return Optional.empty();
        }
    }

    public Optional<WorkOrdersList> getWorkOrdersList(String functionalPosition) {
        return inforEamWorkOrdersExtractor.getWorkOrdersList(functionalPosition);
    }
}
