package ch.cern.be.ics.plcinfo.infoream.dto.general;

import java.util.Collections;

public class InforEamInfo {
    private final String code;
    private final String name;
    private final String assetDescription;
    private final String departmentDesc;
    private final String departmentCode;
    private final EquipmentComponents equipmentComponents;
    private final String rackName;

    public InforEamInfo(String code, String name, String assetDescription, String departmentDesc, String departmentCode, EquipmentComponents equipmentComponents, String rackName) {
        this.code = code;
        this.name = name;
        this.assetDescription = assetDescription;
        this.departmentDesc = departmentDesc;
        this.departmentCode = departmentCode;
        this.equipmentComponents = equipmentComponents;
        this.rackName = rackName;
    }

    public InforEamInfo() {
        this("", "", "No data found", "", "", new EquipmentComponents(Collections.emptyMap()), "");
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAssetDescription() {
        return assetDescription;
    }

    public String getDepartmentDesc() {
        return departmentDesc;
    }

    public EquipmentComponents getChildren() {
        return equipmentComponents;
    }

    public String getRackName() {
        return rackName;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }
}
