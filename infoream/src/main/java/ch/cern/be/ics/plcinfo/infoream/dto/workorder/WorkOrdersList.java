package ch.cern.be.ics.plcinfo.infoream.dto.workorder;

import java.util.*;

public class WorkOrdersList {

	private static final int LATEST_WORK_ORDERS_MAX_LENGTH = 10;

    private final List<WorkOrderData> workOrders;

    public WorkOrdersList(List<WorkOrderData> data) {

		Collections.sort(data, new Comparator<WorkOrderData>() {

			@Override
			public int compare(WorkOrderData w, WorkOrderData t) {
				int wId = Integer.parseInt(w.getWorkOrderId());
				int tId = Integer.parseInt(t.getWorkOrderId());
				return wId > tId ? -1 : wId < tId ? 1 : 0;
			}
		});

        workOrders = data.subList(0, LATEST_WORK_ORDERS_MAX_LENGTH);
    }

    public List<WorkOrderData> getWorkOrders() {
        return workOrders;
    }
}