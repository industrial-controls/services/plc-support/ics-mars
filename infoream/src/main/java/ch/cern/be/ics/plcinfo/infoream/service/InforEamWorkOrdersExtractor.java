package ch.cern.be.ics.plcinfo.infoream.service;


import ch.cern.be.ics.plcinfo.infoream.dto.general.EquipmentComponent;
import ch.cern.be.ics.plcinfo.infoream.dto.general.EquipmentComponents;
import ch.cern.be.ics.plcinfo.infoream.dto.workorder.WorkOrderData;
import ch.cern.be.ics.plcinfo.infoream.dto.workorder.WorkOrdersList;
import ch.cern.cmms.infor.wshub.Credentials;
import ch.cern.cmms.infor.wshub.GridRequestResult;
import ch.cern.cmms.infor.wshub.GridRequestRow;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class InforEamWorkOrdersExtractor {

    private final BiFunction<String, Credentials, Optional<GridRequestResult>> workOrdersLikeProvider;
    private final Function<String, EquipmentComponents> graphDataProvider;
    private final Credentials credentials;

    public InforEamWorkOrdersExtractor(BiFunction<String, Credentials, Optional<GridRequestResult>> workOrdersLikeProvider, Function<String, EquipmentComponents> graphDataProvider, Credentials credentials) {
        this.workOrdersLikeProvider = workOrdersLikeProvider;
        this.graphDataProvider = graphDataProvider;
        this.credentials = credentials;
    }

    public Optional<WorkOrdersList> getWorkOrdersList(String functionalPosition) {
        List<Optional<GridRequestResult>> associatedWorkOrders = queryAssociatedWorkOrdersData(functionalPosition);
        if (anyOperationFailed(associatedWorkOrders)) {
            return Optional.empty();
        } else {
            List<WorkOrderData> workOrdersData = associatedWorkOrders
                    .stream()
                    .map(workOrderWSResults -> workOrderWSResults.map(this::convertWorkOrdersResults)
                            .orElse(Collections.emptyList()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            return Optional.of(new WorkOrdersList(workOrdersData));
        }
    }

    private List<Optional<GridRequestResult>> queryAssociatedWorkOrdersData(String functionalPosition) {
        Optional<GridRequestResult> parentWorkOrders = workOrdersLikeProvider.apply(functionalPosition, credentials);
        List<EquipmentComponent> children = graphDataProvider.apply(functionalPosition).getComponents();
        List<Optional<GridRequestResult>> availableWorkOrders = children
                .stream()
                .map(component -> workOrdersLikeProvider.apply(component.getName(), credentials))
                .collect(Collectors.toList());
        availableWorkOrders.add(parentWorkOrders);
        return availableWorkOrders;
    }

    private boolean anyOperationFailed(List<Optional<GridRequestResult>> availableWorkOrders) {
        return availableWorkOrders
                .stream()
                .anyMatch(workOrder -> !workOrder.isPresent());
    }

    private List<WorkOrderData> convertWorkOrdersResults(GridRequestResult result) {
        return result.getRows().getRow().stream()
                .map(row -> row.getCell()
                        .stream()
                        .map(GridRequestRow.Cell::getValue)
                        .collect(Collectors.toList())
                )
                .map(WorkOrderData::new)
                .collect(Collectors.toList());
    }
}
