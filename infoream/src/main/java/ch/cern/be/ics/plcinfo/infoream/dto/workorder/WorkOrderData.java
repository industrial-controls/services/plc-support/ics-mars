package ch.cern.be.ics.plcinfo.infoream.dto.workorder;

import java.util.List;

public class WorkOrderData {
    private final String status;
    private final String type;
    private final String workOrderId;
    private final String equipmentName;
    private final String description;
    private final String department;
    private final String reportedBy;
    private final String scheduledStart;
    private final String location;
    private final String assignedTo;
    private final String creationDate;


    public WorkOrderData(List<String> data) {
        workOrderId = data.get(8);
        description = data.get(10);
        equipmentName = data.get(12);
        location = data.get(13);
        department = data.get(11);
        status = data.get(12).split("-")[0];
        type = data.get(11).split("-")[0];
        reportedBy = data.get(14);
        scheduledStart = data.get(15);
        assignedTo = data.get(14);
		creationDate = data.get(9);
    }

    public String getStatus() {
        return status;
    }

    public String getType() { return type; }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public String getDescription() {
        return description;
    }

    public String getDepartment() {
        return department;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public String getScheduledStart() {
        return scheduledStart;
    }

    public String getLocation() {
        return location;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

	public String getCreationDate() { return creationDate; }
}
