package ch.cern.be.ics.plcinfo.infoream.service;

import ch.cern.be.ics.plcinfo.infoream.dto.general.EquipmentComponents;
import ch.cern.cmms.infor.wshub.Credentials;
import ch.cern.cmms.infor.wshub.Graph;
import ch.cern.cmms.infor.wshub.Node;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

class InforEamGraphExtractor {

    private final BiFunction<String, Credentials, Optional<Graph>> graphDataProvider;
    private final Credentials credentials;

    InforEamGraphExtractor(BiFunction<String, Credentials, Optional<Graph>> graphDataProvider, Credentials credentials) {
        this.graphDataProvider = graphDataProvider;
        this.credentials = credentials;
    }

    EquipmentComponents readGraphData(String equipmentCode) {
        Map<Node, Graph> childrenData = graphDataProvider.apply(equipmentCode, credentials)
                .map(root -> readExistingChildrenData(root, equipmentCode))
                .orElse(Collections.emptyMap());
        return new EquipmentComponents(childrenData);
    }

    private Map<Node, Graph> readExistingChildrenData(Graph root, String parentCode) {
        return readChildrenData(root, parentCode)
                .entrySet()
                .stream()
                .filter(childGraph -> childGraph.getValue().isPresent())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().get()
                ));
    }

    private Map<Node, Optional<Graph>> readChildrenData(Graph root, String parentCode) {
        Map<String, Node> childNodesById = HierarchyUtils.getChildNodesById(root);
        return HierarchyUtils.getChildrenNodes(root, parentCode, childNodesById)
                .collect(Collectors.toMap(
                        node -> node,
                        node -> graphDataProvider.apply(node.getCode(), credentials)
                ));
    }
}
