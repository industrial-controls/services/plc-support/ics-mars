package ch.cern.be.ics.plcinfo.infoream.service;

import ch.cern.cmms.infor.wshub.Graph;
import ch.cern.cmms.infor.wshub.Node;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HierarchyUtils {

    public static Map<String, Node> getChildNodesById(Graph childrenGraph) {
        return childrenGraph.getNodes().stream()
                .collect(Collectors.toMap(
                        Node::getCode,
                        Function.identity()
                ));
    }

    public static Stream<Node> getChildrenNodes(Graph root, String parentCode, Map<String, Node> childNodesById) {
        return root.getEdges()
                .stream()
                .filter(edge -> edge.getFrom().equals(parentCode))
                .map(edge -> childNodesById.get(edge.getTo()));
    }

}
