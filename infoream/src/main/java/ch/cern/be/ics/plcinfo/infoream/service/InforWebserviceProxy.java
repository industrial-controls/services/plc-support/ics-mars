package ch.cern.be.ics.plcinfo.infoream.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.cmms.infor.wshub.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.ws.BindingProvider;
import java.util.Optional;

@Named
class InforWebserviceProxy {

    private final PLCInfoLogger logger;
    private final InforWSPortType webServicePort = new InforWSService().getInforWSPort();

    @Inject
    InforWebserviceProxy(PLCInfoLogger logger) {
        initializeConnection();
        this.logger = logger;
    }

    void initializeConnection() {
        ((BindingProvider) webServicePort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://cmmsx.cern.ch/WSHub/SOAP");
    }

    Equipment readEquipment(String equipmentCode, Credentials credentials) throws SOAPException {
        return webServicePort.readEquipment(equipmentCode, credentials, null);
    }

    Optional<Graph> readGraph(String equipmentCode, Credentials credentials) {
        EquipmentGraphRequest equipmentGraphRequest = new EquipmentGraphRequest();
        equipmentGraphRequest.setEquipmentCode(equipmentCode);
        try {
            return Optional.of(webServicePort.readEquipmentGraph(equipmentGraphRequest, credentials, null));
        } catch (SOAPException e) {
            logger.warning("Failed to access InforEAM. " + e.getMessage());
            initializeConnection();
            return Optional.empty();
        }
    }

    Optional<GridRequestResult> readWorkOrdersLike(String functionalPosition, Credentials credentials) {
        GridRequest gridRequest = new GridRequest();
        gridRequest.setRowCount("50");
        gridRequest.setDataspyID("2005");
        gridRequest.setCursorPosition("1");
        gridRequest.setGridID("93");
        gridRequest.setGridName("WSJOBS");
        gridRequest.setGridType("LIST");
        GridRequest.GridFilters filters = new GridRequest.GridFilters();
        GridRequestFilter filter = new GridRequestFilter();
        filter.setFieldName("equipment");
        filter.setOperator("BEGINS");
        filter.setFieldValue(functionalPosition);
        filters.getGridFilter().add(filter);
        gridRequest.setGridFilters(filters);
        try {
            return Optional.of(webServicePort.getGridResult(gridRequest, credentials, null));
        } catch (SOAPException e) {
            logger.warning("Failed to access InforEAM. " + e.getMessage());
            initializeConnection();
            return Optional.empty();
        }
    }
}
