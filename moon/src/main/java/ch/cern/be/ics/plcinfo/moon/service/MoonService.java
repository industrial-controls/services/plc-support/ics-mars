package ch.cern.be.ics.plcinfo.moon.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;
import ch.cern.be.ics.plcinfo.moon.domain.provider.MoonInfoProvider;
import ch.cern.be.ics.plcinfo.moon.domain.provider.NullInfoProvider;
import ch.cern.be.ics.plcinfo.moon.dto.MoonData;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.rpc.ServiceException;
import java.util.Map;
import java.util.Optional;

@Named
public class MoonService {
    private final PLCInfoLogger logger;
    private final LanDbJDBCService lanDbJDBCService;
    private final Map<String, MoonInfoProvider> infoProviders;

    @Inject
    private MoonService(PLCInfoLogger logger, LanDbJDBCService lanDbJDBCService, Map<String, MoonInfoProvider> infoProviders) throws ServiceException {
        this.logger = logger;
        this.lanDbJDBCService = lanDbJDBCService;
        this.infoProviders = infoProviders;
    }

    public Optional<MoonData> getDeviceInfo(String name) {
        return Utils.collectAsynchronousData(() -> lanDbJDBCService.getDeviceInfo(name), "landb", logger)
                .flatMap(lanDbInfo -> getDeviceInfo(name, lanDbInfo.getGenericType(), lanDbInfo.getMaker()));
    }

    public Optional<MoonData> getDeviceInfo(String name, String type, String manufacturer) {
        String newType = fixType(name, type);
        return infoProviders.getOrDefault(newType, new NullInfoProvider())
                .getDeviceInfo(name, manufacturer)
                .map(info -> new MoonData(name, newType, info));
    }

    /**
     * FEC is a computer but should be handled more like a PLC, and has it's distinctive type.
     * But just from the type/manufacturer we can't decide if it's a FEC, we depend on the name to be CFC-...
     */
    private String fixType(String name, String type) {
        if (name.toLowerCase().startsWith("cfc-") && type.equalsIgnoreCase("computer")) {
            return "FEC";
        }
        return type;
    }
}
