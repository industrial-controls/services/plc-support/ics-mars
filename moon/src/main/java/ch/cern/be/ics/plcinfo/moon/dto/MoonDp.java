package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonDp {
    private final MoonDpConfig moonDpConfig;
    private final String value;

    public MoonDp(MoonDpConfig moonDpConfig, String value) {
        this.moonDpConfig = moonDpConfig;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public MoonDpConfig getMoonDpConfig() {
        return moonDpConfig;
    }
}
