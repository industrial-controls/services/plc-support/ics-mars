package ch.cern.be.ics.plcinfo.moon.domain.configuration.plc;

import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Named("SIEMENS_CONFIGURATION")
public class SiemensDatapointConfiguration extends BasePlcDatapointConfiguration {

    @Override
    public List<MoonDpConfig> getInfoMetadata() {
        ArrayList<MoonDpConfig> moonDpConfigs = new ArrayList<>(super.getInfoMetadata());
        moonDpConfigs.addAll(Collections.singletonList(new MoonDpConfig("Configuration.IP", "IP")));
        return moonDpConfigs;
    }
}
