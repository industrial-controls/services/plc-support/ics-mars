package ch.cern.be.ics.plcinfo.moon.dto;

import java.util.Collections;

public class MoonDeviceInformation {
    private final MoonInformation info;
    private final MoonMetricsInformation metrics;
    private final MoonColouredState state;

    public MoonDeviceInformation(MoonInformation info, MoonMetricsInformation metrics, MoonColouredState state) {
        this.info = info;
        this.metrics = metrics;
        this.state = state;
    }

    public MoonDeviceInformation() {
        info = new MoonInformation(Collections.emptyList());
        metrics = new MoonMetricsInformation(Collections.emptyList());
        state = new MoonColouredState("Unknown", StatusColourEnum.YELLOW);
    }

    public MoonInformation getInfo() {
        return info;
    }

    public MoonMetricsInformation getMetrics() {
        return metrics;
    }

    public MoonColouredState getState() {
        return state;
    }
}
