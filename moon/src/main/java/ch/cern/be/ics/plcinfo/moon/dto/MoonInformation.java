package ch.cern.be.ics.plcinfo.moon.dto;

import java.util.List;

public class MoonInformation {
    private final List<MoonDp> data;

    public MoonInformation(List<MoonDp> data) {
        this.data = data;
    }

    public List<MoonDp> getData() {
        return data;
    }
}
