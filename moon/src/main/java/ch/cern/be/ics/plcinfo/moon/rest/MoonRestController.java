package ch.cern.be.ics.plcinfo.moon.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.moon.dto.MoonData;
import ch.cern.be.ics.plcinfo.moon.service.MoonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/moon")
public class MoonRestController {
    private final MoonService moonService;

    @Inject
    public MoonRestController(MoonService moonService) {
        this.moonService = moonService;
    }

    @CrossOrigin
    @GetMapping("/deviceInfo")
    public ResponseEntity<MoonData> getDeviceInfo(@RequestParam String[] code) {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(moonService::getDeviceInfo, Arrays.stream(code)));
    }
}
