package ch.cern.be.ics.plcinfo.moon.service;

import OA.Siemens.*;
import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.moon.dto.MoonColouredState;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDp;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;
import ch.cern.be.ics.plcinfo.moon.dto.StatusColourEnum;
import org.apache.axis.AxisEngine;
import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.ser.VectorDeserializerFactory;
import org.apache.axis.encoding.ser.VectorSerializerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.encoding.TypeMapping;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Named
public class MoonWebserviceProxy {

    private final ReportingPortType moonWebService;
    private final PLCInfoLogger logger;
    private final String lang = "en_US.utf8";

    @Inject
    public MoonWebserviceProxy(PLCInfoLogger logger) {
        this.logger = logger;
        moonWebService = initializeConnection();
    }

    private ReportingPortType initializeConnection() {
        ReportingLocator repLoc = new ReportingLocator();
        registerTypes(repLoc);
        String address = "http://cwe-513-vmw307:80";
        repLoc.setReportingEndpointAddress(address);
        try {
            return repLoc.getReporting();
        } catch (ServiceException e) {
            logger.severe(e);
        }
        return null;
    }

    private void registerTypes(ReportingLocator repLoc) {
        QName qName = new QName("http://www.w3.org/2001/XMLSchema", "list");
        AxisEngine axisEngine = repLoc.getEngine();
        TypeMappingRegistry typeMappingRegistry = axisEngine.getTypeMappingRegistry();
        TypeMapping typeMapping = typeMappingRegistry.getOrMakeTypeMapping(org.apache.axis.Constants.URI_SOAP11_ENC);
        typeMapping.register(Object.class, qName, new VectorSerializerFactory(Object.class, qName), new VectorDeserializerFactory(Object.class, qName));
    }

    public List<MoonDp> getDatapointsData(List<MoonDpConfig> dpes) {
        return dpes.parallelStream()
                .map(this::getDatapointData)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<MoonDp> getDatapointData(MoonDpConfig dpe) {
        return getDatapointData(dpe.getDp())
                .map(value -> new MoonDp(dpe, value));
    }

    public Optional<String> getDatapointData(String datapointName) {
        try {
            return dpGet(datapointName)
                    .map(this::convert);
        } catch (RemoteException e) {
            logger.warning(e);
            initializeConnection();
        }
        return Optional.empty();
    }

    private String convert(Object object) {
        if (object instanceof Vector) {
            List<Object> data = (List<Object>) object;
            return data.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining("\n"));
        } else {
            return object.toString();
        }
    }

    public Optional<List<String>> getDatapointDataList(String datapointName) {
        try {
            return dpGet(datapointName)
                    .map(value -> (List<String>) value);
        } catch (RemoteException e) {
            logger.warning(e);
            initializeConnection();
        }
        return Optional.empty();
    }

    /**
     * Convenience method to check for errors in the result.
     */
    private Optional<Object> dpGet(String query) throws RemoteException {
        Optional<Object> returnValue = Optional.of(moonWebService.dpGet(query, lang))
                .filter(result -> result.getErrorCode().equals(BigInteger.valueOf(0)))
                .map(ReturnValue::getValue);
        if (!returnValue.isPresent()) {
            logger.warning("No data returned for '" + query + "'");
        }
        return returnValue;
    }


    public MoonColouredState getStateForPlc(String name) {
        String dpPattern = "*|" + name.toUpperCase() + ".";
        return getStateForPattern(dpPattern);
    }

    public MoonColouredState getStateForHost(String name) {
        String dpPattern = "*|FMC/" + name.toUpperCase() + ".";
        return getStateForPattern(dpPattern);
    }

    private MoonColouredState getStateForPattern(String pattern) {
        String dpType = "_FwFsmDevice";
        Optional<DpInfoValue> dp = getMatchingDpInfo(dpType, pattern);
        return dp.map(this::getState)
                .orElse(new MoonColouredState("UNKNOWN", StatusColourEnum.YELLOW));
    }

    private MoonColouredState getState(DpInfoValue dp) {
        Set<String> goodStates = new HashSet<>(Arrays.asList("OK", "ON", "RUNNING"));
        StatusColourEnum colour = StatusColourEnum.RED;
        String currentState = "UNKNOWN";
        String dpName = dp.getDpName() + "fsm.currentState";
        try {
            currentState = moonWebService.dpGet(dpName, lang).getValue().toString();
            if (goodStates.contains(currentState.toUpperCase())) {
                colour = StatusColourEnum.GREEN;
            } else if (currentState.equalsIgnoreCase("UNKNOWN")) {
                colour = StatusColourEnum.YELLOW;
            }
        } catch (RemoteException e) {
            logger.warning(e);
            initializeConnection();
        }
        return new MoonColouredState(currentState, colour);
    }

    private Optional<DpInfoValue> getMatchingDpInfo(String dpType, String dpPattern) {
        return getMatchingDpInfo(dpType, dpPattern, (dpInfo) -> true);
    }

    public Optional<DpInfoValue> getDpInfo(String dpAlias, String dpType, String dpPattern) {
        return getMatchingDpInfo(dpType, dpPattern, dpInfo -> dpAlias.equalsIgnoreCase(dpInfo.getAlias()));
    }

    public Optional<DpInfoValue> getMatchingDpInfo(String dpType, String dpPattern, Predicate<DpInfoValue> predicate) {
        try {
            Optional<ReturnDpInfoList> dpList = Optional.ofNullable(moonWebService.dpNames(dpPattern, dpType, lang, 0));
            DpInfoValue[] dpInfoValues = dpList.map(ReturnDpInfoList::getTable)
                    .orElse(new DpInfoValue[0]);
            return Arrays.stream(dpInfoValues)
                    .filter(predicate)
                    .findFirst();
        } catch (RemoteException e) {
            logger.warning(e);
            initializeConnection();
        }
        return Optional.empty();
    }

}
