package ch.cern.be.ics.plcinfo.moon.domain.provider;


import OA.Siemens.DpInfoValue;
import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.service.ColourAssigner;
import ch.cern.be.ics.plcinfo.moon.service.MoonWebserviceProxy;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class MoonFrontendProvider implements MoonInfoProvider {
    private final GenericMoonInfoProvider genericMoonInfoProvider;
    private final MoonWebserviceProxy moonWebserviceProxy;

    MoonFrontendProvider(MoonWebserviceProxy moonWebserviceProxy,
                         ColourAssigner colourAssigner,
                         Supplier<String> datapointTypeProvider,
                         Supplier<String> dataValidityProvider,
                         Function<String, GenericDatapointConfiguration> configurationProvider) {
        this.moonWebserviceProxy = moonWebserviceProxy;
        genericMoonInfoProvider = new GenericMoonInfoProvider(moonWebserviceProxy,
                colourAssigner,
                datapointTypeProvider,
                dataValidityProvider,
                this::getRealDatapointName,
                configurationProvider);
    }

    @Override
    public MoonInfoProvider getConcreteProvider() {
        return genericMoonInfoProvider;
    }

    private Optional<String> getRealDatapointName(String name, String type) {
        return moonWebserviceProxy.getDpInfo(name, type, "*.")
                .map(DpInfoValue::getDpName);
    }
}
