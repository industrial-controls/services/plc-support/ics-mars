package ch.cern.be.ics.plcinfo.moon.service;

import ch.cern.be.ics.plcinfo.moon.dto.StatusColourEnum;

import javax.inject.Named;

@Named
public class ColourAssigner {
    public StatusColourEnum getColour(Integer alarmState, boolean dataValid, boolean alarmActive) {
        if (!dataValid) {
            return StatusColourEnum.BLUE;
        } else {
            return getProperColour(alarmState, alarmActive);
        }
    }

    private StatusColourEnum getProperColour(Integer alarmState, boolean alarmActive) {
        if (!alarmActive) {
            return StatusColourEnum.YELLOW;
        } else if (alarmState == 0) {
            return StatusColourEnum.GREEN;
        } else {
            return StatusColourEnum.RED;
        }
    }

}

