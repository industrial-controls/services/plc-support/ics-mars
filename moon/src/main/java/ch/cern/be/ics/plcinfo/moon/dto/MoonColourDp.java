package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonColourDp extends MoonDp {
    private final StatusColourEnum colour;

    public MoonColourDp(MoonDpConfig moonDpConfig, String value, StatusColourEnum colour) {
        super(moonDpConfig, value);
        this.colour = colour;
    }

    public StatusColourEnum getColour() {
        return colour;
    }
}
