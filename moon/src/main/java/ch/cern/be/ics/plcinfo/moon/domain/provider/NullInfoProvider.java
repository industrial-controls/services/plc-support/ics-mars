package ch.cern.be.ics.plcinfo.moon.domain.provider;

import ch.cern.be.ics.plcinfo.moon.dto.MoonDeviceInformation;

import java.util.Optional;

public class NullInfoProvider implements MoonInfoProvider {
    @Override
    public Optional<MoonDeviceInformation> getDeviceInfo(String name, String manufacturer) {
        return Optional.empty();
    }
}
