package ch.cern.be.ics.plcinfo.moon.domain.provider;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.service.ColourAssigner;
import ch.cern.be.ics.plcinfo.moon.service.MoonWebserviceProxy;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.Optional;

@Named("COMPUTER")
public class MoonComputerProvider implements MoonInfoProvider {
    private final GenericMoonInfoProvider genericMoonInfoProvider;

    @Inject
    protected MoonComputerProvider(Map<String, GenericDatapointConfiguration> datapointConfigurations,
                                   MoonWebserviceProxy moonWebserviceProxy,
                                   ColourAssigner colourAssigner) {
        this.genericMoonInfoProvider = new GenericMoonInfoProvider(
                moonWebserviceProxy,
                colourAssigner,
                this::getDatapointType,
                this::getDataValidityDPE,
                this::getRealDatapointName,
                manufacturer -> getConfiguration("COMPUTER", datapointConfigurations)
        );
    }

    @Override
    public MoonInfoProvider getConcreteProvider() {
        return genericMoonInfoProvider;
    }

    private Optional<String> getRealDatapointName(String name, String type) {
        return Optional.of(("fmc/" + name).toUpperCase());
    }

    private String getDataValidityDPE() {
        return ".agentCommunicationStatus.summary";
    }

    private String getDatapointType() {
        return "FwFMCNode";
    }
}
