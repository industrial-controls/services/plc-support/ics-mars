package ch.cern.be.ics.plcinfo.moon.domain.configuration;

import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;

import java.util.List;

public interface GenericDatapointConfiguration {
    List<MoonDpConfig> getInfoMetadata();
}
