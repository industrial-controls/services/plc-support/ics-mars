## Setup instructions:

1. Clone repository
2. Build with Maven via `mvn verify`
    > **NOTE** For the `web` module, Docker needs to be running in the background, and it might be
      necessary to run `export DOCKER_HOST=unix:///var/run/docker.sock`, depending on your platform
3. The final JAR is generated in `/web/target`
4. In order to run you can start the jar via `java -jar xxx.jar` or run `main()` method in `Runner` class in `web` module
5. The application requires some credentials to work properly, and those are collected from environmental variables:
    - `login` - service account login
    - `pass` - password for CERN DB1 for specified login
    - `pass2` - password for Infor, Impact and LanDB for specified login
    - `icesasLogin` - login for IceSAS database 
    - `icesasPassword` - password for IceSAS database

## How to push a Stable image to CERN Gitlab Registry (Warning, changes production version!)

Run the following commands:

```sh
git pull
./mvnw clean package --settings settings.xml
docker login gitlab-registry.cern.ch
cd web/target/
docker tag gitlab-registry.cern.ch/industrial-controls/services/plc-support/ics-mars gitlab-registry.cern.ch/industrial-controls/services/plc-support/ics-mars:stable
docker push gitlab-registry.cern.ch/industrial-controls/services/plc-support/ics-mars:stable
```

 
