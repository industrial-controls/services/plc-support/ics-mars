package ch.cern.be.ics.plcinfo.impact.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityLocationDTO {
    private long id;
    private Set<AccessPointDTO> accessPoints;
    private LocationDTO location;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<AccessPointDTO> getAccessPoints() {
        return accessPoints;
    }

    public void setAccessPoints(Set<AccessPointDTO> accessPoints) {
        this.accessPoints = accessPoints;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}
