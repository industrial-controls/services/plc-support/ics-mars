package ch.cern.be.ics.plcinfo.impact.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.impact.dto.ImpactData;
import ch.cern.be.ics.plcinfo.impact.service.ImpactService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/impact")
public class ImpactRestController {
    private final ImpactService impactService;

    @Inject
    public ImpactRestController(ImpactService impactService) {
        this.impactService = impactService;
    }

    @CrossOrigin
    @GetMapping("/general")
    public ResponseEntity<ImpactData> getImpactData(@RequestParam String userId) {
        return Utils.formResponse(impactService.getData(userId));
    }
}
