package ch.cern.be.ics.plcinfo.impact.dto;

import ch.cern.be.ics.plcinfo.impact.input.AccessPointDTO;

import java.util.ArrayList;
import java.util.List;

public class AccessPointsForLocation {
    private List<AccessPointDTO> generalAccessPoints = new ArrayList<>();
    private List<AccessPointDTO> restrictedAccessPoints = new ArrayList<>();

    public List<AccessPointDTO> getGeneralAccessPoints() {
        return generalAccessPoints;
    }

    public void setGeneralAccessPoints(List<AccessPointDTO> generalAccessPoints) {
        this.generalAccessPoints = generalAccessPoints;
    }

    public List<AccessPointDTO> getRestrictedAccessPoints() {
        return restrictedAccessPoints;
    }

    public void setRestrictedAccessPoints(List<AccessPointDTO> restrictedAccessPoints) {
        this.restrictedAccessPoints = restrictedAccessPoints;
    }
}
