package ch.cern.be.ics.plcinfo.impact.service;

import ch.cern.be.ics.plcinfo.commons.service.RestQueryProxy;
import ch.cern.be.ics.plcinfo.impact.input.PagedList;
import org.springframework.core.ParameterizedTypeReference;

import javax.inject.Named;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Named
public class ImpactQueryProxy {
    private final RestQueryProxy restQueryProxy = new RestQueryProxy("https://impact.cern.ch/impact/api/");

    <T> T query(String userId, String path, Class<T> returnType) {
        return restQueryProxy.query(path, returnType, Function.identity(), getHeadersSupplier(userId));
    }

    <T> List<T> queryPagedResult(String userId, String path, ParameterizedTypeReference<PagedList<T>> parameterType) {
        PagedList<T> response = restQueryProxy.queryParametrizedResult(path, parameterType, Function.identity(), getHeadersSupplier(userId));
        return response.getResult();
    }

    private Supplier<Map<String, String>> getHeadersSupplier(String userId) {
        return () -> Stream.concat(
                restQueryProxy.getBasicAuthHeadersSupplier().get().entrySet().stream(),
                Stream.of(new AbstractMap.SimpleEntry<>("X-Ais-User-Id", userId))
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
