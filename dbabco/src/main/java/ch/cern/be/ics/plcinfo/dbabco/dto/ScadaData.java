package ch.cern.be.ics.plcinfo.dbabco.dto;

public class ScadaData {
    private final String projectName;
    private final String hostName;
    private final String redundantHost;
    private final String unicosApplication;
    private final String deviceName;
    private final String deviceType;
    private final String comments;

    public ScadaData(String projectName, String hostName, String redundantHost, String unicosApplication, String deviceName, String deviceType, String comments) {
        this.projectName = projectName;
        this.hostName = hostName;
        if (hostName.equals(redundantHost)) {
            this.redundantHost = "Not redundant";
        } else {
            this.redundantHost = redundantHost;
        }
        this.unicosApplication = unicosApplication;
        this.deviceName = deviceName;
        this.deviceType = deviceType;
        this.comments = comments;
    }

    public ScadaData() {
        this("No data found", "", "", "", "", "", "");
    }

    public String getProjectName() {
        return projectName;
    }

    public String getHostName() {
        return hostName;
    }

    public String getRedundantHost() {
        return redundantHost;
    }

    public String getUnicosApplication() {
        return unicosApplication;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getComments() {
        return comments;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
