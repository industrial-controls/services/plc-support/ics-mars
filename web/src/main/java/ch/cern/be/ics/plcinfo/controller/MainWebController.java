package ch.cern.be.ics.plcinfo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class MainWebController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView renderSelectionForm() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView renderError() {
        return new ModelAndView("error");
    }
}
