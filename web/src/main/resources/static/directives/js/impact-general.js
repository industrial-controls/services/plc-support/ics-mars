/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("impactGeneral", function() {
    return {
        restrict: 'AE',
        controller: 'ImpactGeneralController',
        templateUrl: 'directives/templates/impact-general.html'
    }
});