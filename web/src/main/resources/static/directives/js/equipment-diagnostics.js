/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("equipmentDiagnostics", function() {
    return {
        restrict: 'AE',
        controller: 'EquipmentDiagnosticsController',
        templateUrl: 'directives/templates/equipment-diagnostics.html'
    }
});