/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("mapLocation", function() {
    return {
        restrict: 'AE',
        controller: 'MapLocationController',
        templateUrl: 'directives/templates/map-location.html'
    }
});