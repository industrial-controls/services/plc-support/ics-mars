/**
 * Created by Eleni Mandilara on 17/7/2017.
 */

app.directive("navigationBar", function() {
    return {
        restrict: 'AE',
        controller: 'SearchController',
        templateUrl: 'directives/templates/navigation-bar.html'
    }
});