/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("icsMars", function() {
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/ics-mars.html'
    }
});