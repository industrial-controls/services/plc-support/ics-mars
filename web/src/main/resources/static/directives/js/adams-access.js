/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("adamsAccess", function() {
    return {
        restrict: 'AE',
        controller: 'AdamsAccessController',
        templateUrl: 'directives/templates/adams-access.html'
    }
});