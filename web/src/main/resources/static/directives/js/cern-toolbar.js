/**
 * Created by Eleni Mandilara on 6/27/2017.
 */

app.directive("cernToolbar", function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'directives/templates/cern-toolbar.html'
    }
});