/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("workOrders", function() {
    return {
        restrict: 'AE',
        controller: 'WorkOrdersController',
        templateUrl: 'directives/templates/work-orders.html'
    }
});