/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("equipmentStructure", function() {
    return {
        restrict: 'AE',
        controller: 'EquipmentStructureController',
        templateUrl: 'directives/templates/equipment-structure.html'
    }
});