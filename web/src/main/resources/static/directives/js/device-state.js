/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("deviceState", function() {
    return {
        restrict: 'AE',
        controller: 'SearchController',
        templateUrl: 'directives/templates/device-state.html'
    }
});