/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("equipmentInfo", function() {
    return {
        restrict: 'AE',
        controller: 'EquipmentInfoController',
        templateUrl: 'directives/templates/equipment-info.html'
    }
});