/**
 * Created by Eleni Mandilara on 18/7/2017.
 */

app.directive("unicosInfo", function() {
    return {
        restrict: 'AE',
        controller: 'UnicosInfoController',
        templateUrl: 'directives/templates/unicos-info.html'
    }
});