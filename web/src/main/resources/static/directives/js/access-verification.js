/**
 * Created by Eleni Mandilara on 25/8/2017.
 */

app.directive("accessVerification", function() {
    return {
        restrict: 'AE',
        controller: 'AccessVerificationController',
        templateUrl: 'directives/templates/access-verification.html'
    }
});