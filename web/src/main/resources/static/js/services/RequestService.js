app.factory('RequestService', function($http) {
	return function(url) {
		return $http({
			method: 'GET',
			url: url
		});
	};
});