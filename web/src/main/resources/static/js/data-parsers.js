/**
 * Created by Eleni Mandilara on 23/10/2017.
 */

/* ******************************************************
 * Callbacks to parse data of AJAX request responses
 * ******************************************************/

parseInforPosition = function(requestSuccess, response) {
    if(requestSuccess){
		console.log("%c Data received from InforPosition about PLC equipment:", 'color: green', response.data);
		var treeData = {
			name : response.data.code,
			children : response.data.children.components[0].children
		};
		return {
				plcData : response.data.children.components[0].children,
				plcDataEquipment : {
					name : response.data.children.components[0].name,
					description : response.data.children.components[0].description,
					treeData : treeData,
					link : "https://cmmsx.cern.ch/SSO/eamlight/position.xhtml?code=" + response.data.code
				},
				plcCode : response.data.code,
				status : "healthy",
				completed : true
		};
	} else {
		logRequestFailure("Infor/Position", response);
		return {
				status : "empty",
				completed : true
		};
	}
};

parseInforWorkOrders = function(requestSuccess, response) {
	if(requestSuccess){
		var workOrders = response.data.workOrders;
        var workOrdersBaseUrl = "https://cmmsx.cern.ch/SSO/eamlight/workorder.xhtml?code=";
        console.log("%c Data received from InforWorkOrders about work orders:", 'color: green', workOrders);
        for(i=0;i<workOrders.length;i++){
            workOrders[i].link = workOrdersBaseUrl + workOrders[i].workOrderId;
        }
        return {
        	workOrders : workOrders,
        	status : "healthy",
        	completed : true
        }
	} else {
		logRequestFailure("Infor/WorkOrders", response);
	    return {
			workOrders : [],
			status : "empty",
			completed : true
		}
	}
};

parseMapLocation = function(requestSuccess, response) {
	if(requestSuccess){
		var isNotEmpty = response.data.mapLocation.split("it=[")[1] ==="'']";
        console.log("%c Data received from Map Service about map rack location:", 'color: green', response.data.mapLocation);
        return {
            url : response.data.mapLocation,
            status : "healthy",
            completed : true
        };
	} else {
		logRequestFailure("CERN Maps", response);
	    return {
			status : "empty",
			completed : true
        };
	}
};

parseImpactGeneral = function(requestSuccess, response) {
	if(requestSuccess){
		var impactData = response.data.data;
		var impactBaseUrl = "https://impact.cern.ch/impact/secure/?place=editActivity:";
		console.log("%c Data received from ImpactGeneral about IMPACT requests:", 'color: green', impactData);
		for(i=0;i<impactData.length;i++){
			impactData[i].link = impactBaseUrl + impactData[i].id;
		}
		return {
			impactData : impactData,
			status : "healthy",
			completed : true
		}
	} else {
		logRequestFailure("IMPACT", response);
		return {
			status : "empty",
			completed : true
		}
	}

};

parseLanDb = function(requestSuccess, response) {
	if(requestSuccess){
		var data = response.data;
		console.log("%c Data received from Landb about equipment info:", 'color: green',data);
		return {
			hostName : data.hostName,
			description : data.description,
			equipmentType : data.genericType,
			deviceMaker : data.maker,
			hardwareType : data.hardwareType,
			location : data.location,
			outlet : data.outlet,
			equipmentDescription : data.description,
			responsiblePerson : data.responsiblePerson,
			mainUser : data.mainUser,
			link : "https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForDisplay&DeviceName=" + data.hostName,
			status : "healthy",
			completed : true
		}
	} else {
		logRequestFailure("LanDB", response);
		return {
			status : "empty",
			completed : true
    	}
	}
};

parseMoonDeviceInfo = function(requestSuccess, response) {

	if(requestSuccess){
		var moonDeviceInformation = response.data;
        console.log("%c Data received from MOON about equipment diagnostics (alarms, PLC time & PLC diagnostic buffer):", 'color: green', moonDeviceInformation);
        var stateTextColor = "black";
        if((moonDeviceInformation.moonDeviceInformation.state.colour==="GREEN")||( moonDeviceInformation.moonDeviceInformation.state.colour==="RED")){
            stateTextColor = "white";
        }
        var deviceStateInfo = {
        	name : moonDeviceInformation.name,
            state : moonDeviceInformation.moonDeviceInformation.state.state,
            stateColour : { 'color': stateTextColor, 'background': moonDeviceInformation.moonDeviceInformation.state.colour}
        };

        var metrics = moonDeviceInformation.moonDeviceInformation.metrics.values;
        var diagnostics = [];
        for(i=0;i<metrics.length;i++){
            diagnostics.push(
                {
                    "description" : metrics[i].moonDpConfig.description,
                    "color" : { "background" : metrics[i].colour }
                }
            )
        }

        var plcData = moonDeviceInformation.moonDeviceInformation.info.data;
        for(i=0;i<plcData.length;i++){
        	var plcDiagnostics = [];
        	if(i==1){
        		var moonDpConfig = plcData[i].value.split("<br>");
        		for(j=0; j<moonDpConfig.length; j++){
					plcDiagnostics.push({
						"description" : moonDpConfig[j].split(":")[0],
                        "value" : moonDpConfig[j].split(":")[1]
					})
        		}
        	} else {
        		var plcMetric = {
					"description" : plcData[i].moonDpConfig.description,
					"value" : plcData[i].value
				}
        	}
        }
        var plcInfo = {
        	plcDiagnostics : plcDiagnostics,
        	plcMetric : plcMetric
        }
        return {
            info : deviceStateInfo,
            diagnostics : diagnostics,
            plcInfo : plcInfo,
            link : "https://moon.web.cern.ch/moon/",
            status : "healthy",
    		completed : true
        }
	} else {
		logRequestFailure("MOON", response);
	    return {
			status : "empty",
			completed : true
		}
	}
};

parseAdamsAccess = function(requestSuccess, response, userId) {
	if(requestSuccess) {
		var adamsBaseUrl = "https://oraweb.cern.ch/ords/devdb11/adams3/api/situationatacp/";
        var data = response.data;
        console.log("%c Data received from AdamsAccess about access points:", 'color: green', data);
        var accessPointsInformationParsed = data.accessPointsInformation;
        for(i=0; i<accessPointsInformationParsed.length; i++ ){
            accessPointsInformationParsed[i].accessibility = setAccessLabel(accessPointsInformationParsed[i].granted);
            accessPointsInformationParsed[i].link = adamsBaseUrl + userId + "/" + accessPointsInformationParsed[i].accessPoint.accesspointcode + "/Y";
        }
        return {
        	points : accessPointsInformationParsed,
        	status : "healthy",
    		completed : true
        }
	} else {
		logRequestFailure("ADaMS", response);
	    return {
			status : "empty",
			completed : true
		}
	}
};

parseIceSasGeneral = function(requestSuccess, response) {
	if(requestSuccess){
		var data = response.data;
		var comments = data.comments.replace(/\s/g, "").replace(/\[/g,'[{').replace(/\]/g,'}]').replace(/\|/g," | ");
		comments = JSON.stringify(eval("(" + comments + ")"));
		comments = JSON.parse(comments);
		console.log("%c Data received from IceSas about UNICOS info (application & resources):", 'color: green', data);
		comments.Application[0].PLCApplicationVersion = toWinCCFormat(comments.Application[0].PLCApplicationVersion);
		if(comments.Application[0].ImportDate.substring("|")!==-1){
			var importDatesArr = comments.Application[0].ImportDate.replace(/\s/g, "").split("|");
			comments.Application[0].ImportDate = "";
			for(i=0;i<importDatesArr.length;i++){
				importDatesArr[i] = importDatesArr[i].substring(0, 10) + " " + importDatesArr[i].substring(10, importDatesArr[i].length);
				if(i===importDatesArr.length-1){
					comments.Application[0].ImportDate += importDatesArr[i];
				} else {
					comments.Application[0].ImportDate += importDatesArr[i] + " | ";
				}
			}
		} else {
			comments.Application[0].ImportDate = comments.Application[0].ImportDate.substring(0, 10) + " " + comments.Application[0].ImportDate.substring(10, comments.Application[0].ImportDate.length);
		}
		comments.Resources[0].PLCBaseline = toWinCCFormat(comments.Resources[0].PLCBaseline);

		return {
			unicosHost : data.hostName,
			winccoaProject : data.projectName,
			redundantHost : data.redundantHost,
			application : comments.Application,
			resources : comments.Resources,
			status : "healthy",
			completed : true
		}

	} else {
		logRequestFailure("IceSas", response);
		return {
			status : "empty",
			completed : true
		}
	}
};

parseIdentifierPosition = function(response) {
    console.log("%c Data received from IdentifierPosition about potential identifiers:", 'color: green', response.data);
    return response.data;
};