/**
 * Created by Eleni Mandilara on 23/10/2017.
 */

/* ******************************************************
 * Change URL state to 'equipment'
 * with parameter 'code' and its value
 * ******************************************************/

changeState = function (state, searchBy) {
    searchBy = searchBy.toUpperCase();
    state.go('equipment', { code: searchBy });
};

/* ******************************************************
 * Set styles and tooltips about the health of
 * the data sources and the access rights
 * ******************************************************/

setDecoration = {
	style : function() {
		return {
			pending : {
				color: "white",
				background: "#d0cbcb"
			},
			healthy : {
				color: "white",
				background: "green"
			}
			,
			unknown : {
				color: "white",
				background: "grey"
			},
			empty : {
				color: "white",
				background: "#5d5959"
			}
		}
	},
	tooltip : function(){
		return {
			pending : "Pending",
			healthy : "OK",
			unknown : "Unknown",
			empty : "Unsuccessful request"
    	};
	}
};

setAccessLabel = function(access) {
    if(access){
        return {
            text : "Access granted",
            style : setHealthStatus(true).style
        }
    } else {
        return {
            text: "Access not granted",
            style: {"color": "white", "background": "red"}
        }
    }
};


/* ******************************************************
 * Auxiliary functions
 * ******************************************************/

roundTo = function(value, digits) {
    if (digits === undefined) {
        digits = 0;
    }

    var multiplicator = Math.pow(10, digits);
    value = parseFloat((value * multiplicator).toFixed(11));
    var test =(Math.round(value) / multiplicator);
    return +(test.toFixed(digits));
};

toWinCCFormat = function(value) {
    var rounded = roundTo(value,3);
    return parseFloat(rounded).toFixed(3);
};


/* ******************************************************
 * Log reason of AJAX request failure
 * ******************************************************/
logRequestFailure = function(source, reason) {
	console.log("%c Request to data source '%s' failed with reason: %s (%s)", 'color: purple', source, reason.statusText, reason.status);
};