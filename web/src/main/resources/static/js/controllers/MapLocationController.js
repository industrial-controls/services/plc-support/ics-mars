/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('MapLocationController', function ($scope, $stateParams, RequestService) {

    $scope.mapLocation = '';

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "mapLocation" : true }
	};

	$scope.toggleCollapsibleMap = function(){
		$scope.decoration.collapsed.mapLocation = !$scope.decoration.collapsed.mapLocation;
	};

	RequestService($scope.apiInfo.map.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.map.callbacks.success(response) },
		function(reason) { $scope.apiInfo.map.callbacks.failure(reason) }
	);

});