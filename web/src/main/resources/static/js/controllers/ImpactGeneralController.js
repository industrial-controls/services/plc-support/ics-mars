/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('ImpactGeneralController', function ($scope, $stateParams, RequestService) {

	var userId = $scope.userInfo.userId;

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "impactGeneral" : true }
	};

	$scope.toggleCollapsibleImpact = function(){
		$scope.decoration.collapsed.impactGeneral = !$scope.decoration.collapsed.impactGeneral;
	};

	RequestService($scope.apiInfo.impact.url + $stateParams.code + '&userId='+ userId).then(
		function(response) { $scope.apiInfo.impact.callbacks.success(response) },
		function(reason) { $scope.apiInfo.impact.callbacks.failure(reason) }
	);

});