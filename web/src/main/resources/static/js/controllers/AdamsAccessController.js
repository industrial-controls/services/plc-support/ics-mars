/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('AdamsAccessController', function ($scope, $stateParams, RequestService) {

	var userId = $scope.userInfo.userId;

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "adamsAccess" : true }
	};

	$scope.toggleCollapsibleAdams = function(){
		$scope.decoration.collapsed.adamsAccess = !$scope.decoration.collapsed.adamsAccess;
	};

	RequestService($scope.apiInfo.adams.url + $stateParams.code + '&userId='+ userId).then(
		function(response) { $scope.apiInfo.adams.callbacks.success(response, userId) },
		function(reason) { $scope.apiInfo.adams.callbacks.failure(reason) }
	);

});