/**
 * Created by Eleni Mandilara on 11/7/2017.
 */

app.controller('SearchController', function ($scope, $state, $stateParams, $rootScope, $window, RequestService) {

	$scope.searchFormData = { equipmentName : "", equipmentId : "" };
	$scope.searchEquipmentModalHeader = "Display equipment";
	$scope.searchInstructions = "You can display an equipment by entering its name or ID.";
	$scope.searchLabel = { byId : "Enter ID: ", byName: "Enter name: " };
	$scope.searchFormSuccess = false;
	$scope.searchFormVisible = true;
	$scope.searchForm = {
		data : { equipmentName : '', equipmentId : '' },
		placeholder : { equipmentName : "Equipment name", equipmentId : "Equipment ID" }
	};

	$scope.deviceInformation = {
		name : "Unknown",
		state : "Unknown",
		stateColour : { "background": "grey" },
		link : "https://icedm.web.cern.ch/iCEDM/?code="
	};
	$scope.userInfo = {};
	$scope.code =  $rootScope.$stateParams.code;

	$scope.equipmentNameFound = false;

	$scope.$on('dataSourceLoaded',  function(event, data){
		if((!$scope.equipmentNameFound) || (data.source == "moon")){
			switch(data.source){
				case "moon":
					$scope.deviceInformation.name = $scope.moonData.info.name;
					$scope.deviceInformation.state = $scope.moonData.info.state;
					$scope.deviceInformation.stateColour = $scope.moonData.info.stateColour;
					break;
				case "lanDb":
					$scope.deviceInformation.name = $scope.info.hostName;
					break;
				case "inforPosition":
					$scope.deviceInformation.name = $scope.plcInfo.plcCode;
					break;
			}
			$scope.deviceInformation.link += $scope.deviceInformation.name;
			$scope.equipmentNameFound = true;
		}
	});

	$scope.apiInfo = {
		infor : {
			url : 'api/infor/position?code=' ,
			callbacks: {
				success : function(response){
					$scope.plcInfo = parseInforPosition(true, response);
					$scope.toggleCollapsibleStructure();
					$scope.collapseInfor = "in";
					$scope.$emit('dataSourceLoaded', { source : "inforPosition" });
				},
				failure : function(response){
					$scope.plcInfo = parseInforPosition(false, response);
				}
			}
		},
		lanDb : {
			url : 'api/landb/general?code=',
			callbacks: {
				success : function(response){
					$scope.info = parseLanDb(true, response);
					$scope.toggleCollapsibleInfo();
					$scope.collapseLanDb = "in";
					$scope.$emit('dataSourceLoaded', { source : "lanDb" });
				},
				failure : function(response){
					$scope.info = parseLanDb(false, response);
				}
			}
		},
		moon : {
			url : 'api/moon/deviceInfo?code=',
			callbacks: {
				success : function(response){
				  	$scope.moonData = parseMoonDeviceInfo(true, response);
					$scope.toggleCollapsibleDiagnostics();
					$scope.collapseMoon = "in";
					$scope.$emit('dataSourceLoaded', { source : "moon" });
				},
				failure : function(response){
                    $scope.moonData = parseMoonDeviceInfo(false, response);
				}
			}
		},
		unicos : {
			url : 'api/icesas/general?code=',
			callbacks: {
				success : function(response){
					$scope.unicosInfo = parseIceSasGeneral(true, response);
					$scope.toggleCollapsibleUnicos("unicosInfo");
					$scope.collapseUnicos = "in";
				},
				failure : function(response){
					$scope.unicosInfo = parseIceSasGeneral(false, response);
				}
			}
        },
        map : {
        	url : 'api/map/location?code=',
			callbacks: {
				success : function(response){
					$scope.mapLocation = parseMapLocation(true, response);
					$scope.toggleCollapsibleMap();
					$scope.collapseMap = "in";
				},
				failure : function(response){
					$scope.mapLocation = parseMapLocation(false, response);
				}
			}
        },
        workOrders : {
        	url : 'api/infor/workOrders?code=',
			callbacks: {
				success : function(response){
					$scope.workOrders = parseInforWorkOrders(true, response);
					$scope.toggleCollapsibleWorkOrders();
					$scope.collapseWorkOrders = "in";
				},
				failure : function(response){
					$scope.workOrders = parseInforWorkOrders(false, response);
				}
			}
        },
        adams : {
        	url : 'api/adams/access?code=',
			callbacks: {
				success : function(response, userId){
					$scope.accessPointsInformation = parseAdamsAccess(true, response, userId);
					$scope.toggleCollapsibleAdams();
					$scope.$emit('adamsLoaded', { loaded : true , success : true});
					$scope.collapseAdams = "in";
				},
				failure : function(response){
					$scope.accessPointsInformation = parseAdamsAccess(false, response);
					$scope.$emit('adamsLoaded', { loaded : true , success : false});
				}
			}
        },
        impact : {
        	url : 'api/impact/general?code=',
			callbacks: {
				success : function(response){
					$scope.impactGeneralData = parseImpactGeneral(true, response);
					$scope.toggleCollapsibleImpact();
					$scope.$emit('impactLoaded', { loaded : true, success : true });
					$scope.collapseImpact = "in";
				},
				failure : function(response){
					$scope.impactGeneralData = parseImpactGeneral(false, response);
					$scope.$emit('impactLoaded', { loaded : true, success : false });
				}
			}
        },
        user : {
			url: '/api/users/sso',
			success: function(response){
				$scope.enableView = true;
				$scope.userInfo = response.data;
			}
		},
		identifierAsset : {
		 	url: 'api/identifier/asset?code=',
			success : function(response){
				$scope.searchForm.data.equipmentName = response.data.potentialIdentifiers[0];
				$scope.searchBy = $scope.searchForm.data.equipmentName;
				$scope.searchBy = $scope.searchBy.toUpperCase();
			}
		},
		 identifierPosition : {
			url : 'api/identifier/position?code=',
        }
	};
	// Initialise variables
	$scope.enableView = false;
	$scope.userInfo = {};

	// Get user info
	RequestService($scope.apiInfo.user.url).then(
		function(response) { $scope.apiInfo.user.success(response) },
		function(reason) {}
	);

    $scope.userInfoUrl = 'api/users/userInfo?surname=';
    $scope.userId;

	// Search for equipment by name or ID
    $scope.searchForEquipment = function() {

		// Reload the window when a new search has been triggered only if the state parameters have changed
		if (($stateParams.code) && ($stateParams.code !== $scope.searchForm.data.equipmentName)){
			$window.location.reload();
		};

        // If ID is provided, then find equipment name
		if($scope.searchForm.data.equipmentId) {
			RequestService($scope.apiInfo.identifierAsset.url + $scope.searchForm.data.equipmentId).then(
        		function(equipmentData) {
					$scope.searchForm.data.equipmentName = equipmentData.data.potentialIdentifiers[0];
					$scope.searchBy = $scope.searchForm.data.equipmentName;
					changeState($state, $scope.searchForm.data.equipmentName);
        		},
        		function(reason) {
        			console.log(reason);
        		}
        	);
		} else {
			// If name is provided then change state with name as code parameter
			changeState($state, $scope.searchForm.data.equipmentName);
		}
    };

    $scope.fetchEquipmentData = function(){
        $scope.fetchDataTriggered = true;

		RequestService($scope.apiInfo.user.url).then(
			function(userInfo) {
				$scope.userInfo = userInfo.data;
				if($scope.searchForm.data.equipmentId){
					console.log("Searching by ID: "+$scope.searchForm.data.equipmentId, $scope.userInfo);

					RequestService($scope.apiInfo.identifierAsset.url + $scope.searchForm.data.equipmentId).then(
						function(equipmentData) {
							$scope.searchForm.data.equipmentName = equipmentData.data.potentialIdentifiers[0];
							$scope.searchBy = $scope.searchForm.data.equipmentName;
						},
						function(reason) {}
					);
				} else {
					$scope.searchBy = $scope.searchForm.data.equipmentName || $stateParams.code;
				}
			},
			function(reason) { console.log(reason) }
		)
    };

    if(($scope.code)&&(!$scope.fetchDataTriggered)){
        $scope.fetchEquipmentData();
    }
});