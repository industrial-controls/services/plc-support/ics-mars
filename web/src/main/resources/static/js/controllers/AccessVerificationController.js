/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('AccessVerificationController', function ($scope, $stateParams, $http) {

	$scope.requestStatus = {
		loading : { accessVerification : false },
		completed : { accessVerification : false },
    };

	var userId = $scope.userInfo.userId;
    $scope.searchAccessVerification = false;
    $scope.unknownUserMsg = false;

 	$scope.resetAccessVerificationForm = function() {
        $scope.searchAccessVerification = false;
        $scope.unknownUserMsg = false;
        $scope.accessPointsInformation = {};
        $scope.impactGeneralData = {};
        $scope.userAccessVerificationId = undefined;
    };

	$scope.adamsLoaded = { loaded : false, success : false };
	$scope.impactLoaded = { loaded : false, success : false };

 	$scope.accessVerificationStatus = 'pending';

    $scope.decorate = function(event, data){
		switch(event.name){
			case "adamsLoaded":
				$scope.adamsLoaded = { loaded: data.loaded, success: data.success };
				break;
			case "impactLoaded":
				$scope.impactLoaded = { loaded: data.loaded, success: data.success };
				break;
		}
		if( $scope.adamsLoaded.loaded && $scope.impactLoaded.loaded) {
			if( $scope.adamsLoaded.success && $scope.impactLoaded.success) {
				$scope.accessVerificationStatus = "healthy";
			} else {
				$scope.accessVerificationStatus = "unknown"
			}
		}
    };

    $scope.$on('adamsLoaded', $scope.decorate);
    $scope.$on('impactLoaded', $scope.decorate);

	$scope.apiInformation = {
		adams: {
			url : 'api/adams/access?code=',
			callbacks: {
				success : function(response){
					$scope.accessPointsInformation = parseAdamsAccess(response, $scope.userAccessVerificationId);
					$scope.requestStatus.loading.adamsAccess = false;
					$scope.requestStatus.completed.adamsAccess = true;
				},
				failure : function(reason){
					$scope.requestStatus.loading.adamsAccess = false;
				}
			}
		},
		impact : {
			url : 'api/impact/general?code=',
			callbacks: {
				success : function(response){
					$scope.impactGeneralData = parseImpactGeneral(response);
					$scope.requestStatus.loading.impactGeneral = false;
					$scope.requestStatus.completed.impactGeneral = true;
				},
				failure : function(reason){
					$scope.requestStatus.loading.impactGeneral = false;
				}
			}
		}
	};

    $scope.verifyAccess = function() {

        $scope.unknownUserMsg = false;

        if($scope.userAccessVerificationId){

            var searchBy = $scope.searchForm.data.equipmentName || $stateParams.code;
            $scope.createGetRequest($scope.apiInformation.adams.url + $stateParams.code + '&userId='+ $scope.userAccessVerificationId , $scope.apiInformation.adams.callbacks.success, $scope.apiInformation.adams.callbacks.failure);
            $scope.createGetRequest($scope.apiInformation.impact.url + $stateParams.code + '&userId='+ $scope.userAccessVerificationId , $scope.apiInformation.impact.callbacks.success, $scope.apiInformation.impact.callbacks.failure);

        } else {
            userAccessVerificationSurname = $scope.userAccessVerificationSurname.toLowerCase();
            $http({
                method: 'GET',
                url: $scope.userInfoUrl + userAccessVerificationSurname
            }).then(
                function (response) {
                    var searchBy = $scope.searchForm.data.equipmentName || $stateParams.code;
                    $scope.userAccessVerificationId = response.data.id;
                    $scope.userAccessVerificationId = parseInt($scope.userAccessVerificationId);
                    $scope.createGetRequest($scope.apiInformation.adams.url + $stateParams.code + '&userId='+ $scope.userAccessVerificationId , $scope.apiInformation.adams.callbacks.success, $scope.apiInformation.adams.callbacks.failure);
                    $scope.createGetRequest($scope.apiInformation.impact.url + $stateParams.code + '&userId='+ $scope.userAccessVerificationId , $scope.apiInformation.impact.callbacks.success, $scope.apiInformation.impact.callbacks.failure);
                }
            ).catch(function (reason) {
                $scope.unknownUserMsg = true;
                $scope.impactGeneralData = {};
            });
        }
        $scope.searchAccessVerification = true;
    };

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "accessVerification" : false }
	};

	$scope.toggleCollapsibleAccessVerification = function(){
		$scope.decoration.collapsed.accessVerification = !$scope.decoration.collapsed.accessVerification;
	};

    $scope.link = { accessVerification : "" };

	$scope.createGetRequest = function(url, successCallbackFn, errorCallbackFn){
		$scope.requestStatus.loading = true;
		$http({
			method: 'GET',
			url: url
		}).then(
			successCallbackFn
		).catch(function (reason) {
			errorCallbackFn(reason)
		});
	};

});