/**
 * Created by Eleni Mandilara on 5/10/2017.
 */

app.controller('BarcodeController', function ($scope, $state, $stateParams, $http, $rootScope) {
    $scope.identifierAssetApiUrl = 'api/identifier/asset?code=';
    $scope.equipmentName = 'Unknown device';
    var params = {
        id: $rootScope.$stateParams.id
    };
    $http({
        method: 'GET',
        url: $scope.identifierAssetApiUrl + params.id
    }).then(
        function (equipmentData) {
            $scope.equipmentName = equipmentData.data.potentialIdentifiers[0];
            changeState($state, $scope.equipmentName);
        }
    ).catch(function (reason) {
       console.log("Unknown equipment ID", reason)
    });
});