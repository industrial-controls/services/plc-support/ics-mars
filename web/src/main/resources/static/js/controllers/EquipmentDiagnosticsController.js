/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('EquipmentDiagnosticsController', function ($scope, $stateParams, RequestService) {

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "equipmentDiagnostics" : true },
		hideModules : true
	};

	$scope.toggleCollapsibleDiagnostics = function(){
		$scope.decoration.collapsed.equipmentDiagnostics = !$scope.decoration.collapsed.equipmentDiagnostics;
	};

	RequestService($scope.apiInfo.moon.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.moon.callbacks.success(response) },
		function(reason) { $scope.apiInfo.moon.callbacks.failure(reason) }
	);

});