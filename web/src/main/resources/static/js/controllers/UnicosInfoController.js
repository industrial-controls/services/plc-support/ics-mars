/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('UnicosInfoController', function ($scope, $stateParams, RequestService) {

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : {
			unicosInfo : true,
			unicosInfoApplication : true,
			unicosInfoResources : true
		}
	};

	$scope.toggleCollapsibleUnicos = function(panel){
		switch(panel){
			case "unicosInfo":
				$scope.decoration.collapsed.unicosInfo = !$scope.decoration.collapsed.unicosInfo;
				break;
			case "unicosInfoApplication":
				$scope.decoration.collapsed.unicosInfoApplication = !$scope.decoration.collapsed.unicosInfoApplication;
				break;
			case "unicosInfoResources":
				$scope.decoration.collapsed.unicosInfoResources = !$scope.decoration.collapsed.unicosInfoResources;
				break;
		}
	};

	RequestService($scope.apiInfo.unicos.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.unicos.callbacks.success(response) },
		function(reason) { $scope.apiInfo.unicos.callbacks.failure(reason) }
	);
});