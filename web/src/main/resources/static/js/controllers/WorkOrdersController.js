/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('WorkOrdersController', function ($scope, $stateParams, RequestService) {

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "workOrders" : true }
	};
	$scope.toggleCollapsibleWorkOrders = function(){
		$scope.decoration.collapsed.workOrders = !$scope.decoration.collapsed.workOrders;
	};

	RequestService($scope.apiInfo.workOrders.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.workOrders.callbacks.success(response) },
		function(reason) { $scope.apiInfo.workOrders.callbacks.failure(reason) }
	);
});