/**
 * Created by Eleni Mandilara on 6/7/2017.
 */

app.controller('HomeController', function($scope, $http) {
    $scope.projectTitle = 'MARS';
    $scope.projectDescription = 'Maintenance and Assets for Controls';
    $scope.paragraph1Header = 'About MARS';
    $scope.paragraph2Header = 'Getting started';
    $scope.paragraph3Header = 'Barcode scanner';

    $scope.paragraph1Content = 'MARS, standing for Maintenance and Assets for Controls, is a web-based tool that federates data from heterogeneous data sources with the aim of providing support for interventions and maintenance activities. The application provides access to this information through a user web interface, a mobile application and a RESTful API. MARS is developed and maintained by the BE-ICS group.';
    $scope.paragraph2Content = 'To get started, type an equipment name in the top search bar. In order to display an equipment by entering its ID, please visit the home view by clicking on "MARS" in the navigation bar. To report a bug, please send an e-mail to';
    $scope.supportEmail = 'industrial-controls-mars@cern.ch';

   	$scope.paragraph3Content = 'For mobile devices, displaying an equipment in MARS by scanning its barcode is supported.'
   	$scope.mobileApplicationLink = '';

});