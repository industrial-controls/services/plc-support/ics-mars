/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('EquipmentInfoController', function ($scope, $stateParams, RequestService) {

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "equipmentInfo" : true }
	};

	$scope.toggleCollapsibleInfo = function(){
		$scope.decoration.collapsed.equipmentInfo = !$scope.decoration.collapsed.equipmentInfo;
	};

	RequestService($scope.apiInfo.lanDb.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.lanDb.callbacks.success(response) },
		function(reason) { $scope.apiInfo.lanDb.callbacks.failure(reason) }
	);

});