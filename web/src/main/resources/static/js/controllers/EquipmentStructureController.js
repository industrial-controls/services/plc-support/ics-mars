/**
 * Created by Eleni Mandilara on 8/3/2018.
 */

app.controller('EquipmentStructureController', function ($scope, $stateParams, RequestService) {

	$scope.decoration = {
		style: setDecoration.style(),
		tooltip: setDecoration.tooltip(),
		collapsed : { "equipmentStructure" : true }
	};

	$scope.toggleCollapsibleStructure = function(){
		$scope.decoration.collapsed.equipmentStructure = !$scope.decoration.collapsed.equipmentStructure;
	};

	RequestService($scope.apiInfo.infor.url + $stateParams.code).then(
		function(response) { $scope.apiInfo.infor.callbacks.success(response) },
		function(reason) { $scope.apiInfo.infor.callbacks.failure(reason) }
	);

});