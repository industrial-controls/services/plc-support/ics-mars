/**
 * Created by Eleni Mandilara on 11/7/2017.
 */

var app = angular.module('app', ['ui.router', 'ngAnimate']);

app.filter('trusted', ['$sce', function ($sce) {
    return $sce.trustAsResourceUrl;
}]);

app.run( ['$rootScope', '$state', '$stateParams',
    function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }
]);

app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
}]);
app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('search', {
            url: '/search',
            templateUrl: 'views/search.html',
            controller: 'SearchController'
        })
         .state('home', {
				url: '/home',
				templateUrl: 'views/home.html',
				controller: 'HomeController'
			})
        .state('equipment', {
                url: '/equipment?code',
                templateUrl: 'views/equipment.html',
                controller: 'SearchController',
                params: {
                    code: { dynamic: true }
                }
            })
        .state('barcode', {
                url: '/barcode?id',
                controller: 'BarcodeController',
                params: {
                    id: { dynamic: true }
                }
            });
        $urlRouterProvider.when('', '/search');
        $urlRouterProvider.otherwise('/search');
});